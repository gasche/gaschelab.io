let commits_rather_than_lines = true

let () = print_string {html|
<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
|html};;

let (jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec)
=   (0,1,2,3,4,5,6,7,8,9,10,11)

let my_data = [
  ("3.09", (2004, jul), (328, 60080));
  ("3.10", (2005, oct), (585, 288408));
  ("3.11", (2007, may), (518, 99197));
  ("3.12", (2008, dec), (501, 225904));
  ("4.00", (2010, jul), (825, 148572));
  ("4.01", (2012, jul), (975, 160144));
  ("4.02", (2013, sep), (1053, 289788));
]
let now = (2014, aug)

let () = (* header *)
  my_data
  |> List.map (fun (release, _d, _c) -> release)
  |> List.map (Printf.sprintf "'%s'")
  |> String.concat ", "
  |> Printf.printf "['', %s],"

let release_months (release, date, (commits_nb, lines_changed))
                   (next_release, future) =
  let months (yr, mnth) = yr*12 + mnth in
  let length = months next_release - months date in
  let header =
    "''" :: List.map (fun _ -> "0") my_data in
  let commits_over_months = 
    Array.to_list @@ Array.init length (fun _ ->
      let metric =
        if commits_rather_than_lines
        then commits_nb else lines_changed in
      let count = Printf.sprintf "%.2f"
        (float metric /. float length) in
      "''" :: List.map (fun (r, _, _) ->
        if r = release then count else "0") my_data
    ) in (date, header :: commits_over_months @ future)

let months = List.fold_right release_months my_data (now, []) |> snd

let () = List.iter (fun li ->
  li |> String.concat ", " |> Printf.printf "[%s],\n")
  months

let () = print_string {html|
        ]);

        var options = {
          title: '|html};;

let () = print_string (if commits_rather_than_lines
  then
    "Average commits per month for each release"
  else
    "Average lines changed per month for each release"
)

let () = print_string
{html|',
          hAxis: {title: '',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>
|html};;

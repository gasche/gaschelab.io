<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Gabriel Scherer</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>

<h1>Gabriel Scherer: Research themes</h1>

<img src="Gabriel_Scherer.jpg" style="float:right; margin:1em;" alt="My picture"
     title="Photo by Jonathan Protzenko"/>

<ul class="menu">
<li><a href="index.html">Index</a></li>
<li><a href="themes.html">Research themes</a></li>
<li><a href="publications.html">Publications</a></li>
<li><a href="service.html">Community service</a></li>
<li><a href="teaching.html">Teaching</a></li>
<li><a href="talks.html">Talks</a></li>
<li><a href="open_access.html">Open Access</a></li>
<li><a href="writings.html">Popularization</a></li>
<li><a href="software.html">Software</a></li>
</ul>

<p>A broad overview of a set of research themes I work on. The
descriptions and pointers are not meant to be exhaustive -- for this,
see the other dedicated pages.</p>

<h2>Quick access</h2>
<ul>
  <li><a href="#unique-inhabitants">Types having a unique inhabitant</a></li>
  <li><a href="#eta-sums">Eta-equivalence with sums</a></li>
  <li><a href="#full-reduction">Full reduction in the face of absurdity</a></li>
  <li><a href="#gadts-subtyping">Meta-theory of mixing GADTs and subtyping in OCaml</a></li>
  <li><a href="#mltt">Formalizing dependent types using logical relations</a></li>
  <li><a href="#mlf">MLF type inference and type-level computation</a></li>
  <li><a href="#macaque">A type-safe database library using phantom types</a></li>
</ul>

<h2 id="unique-inhabitants">Types having a unique inhabitant</h2>

<p>I'm currently working on the question of knowing whether a given
type T has a unique inhabitant -- modulo a given notion of program
equivalence. I wrote two blog posts
(<a href="http://gallium.inria.fr/blog/singleton-types-for-code-inference">part
1</a>,
and <a href="http://gallium.inria.fr/blog/singleton-types-for-code-inference-2">part
2</a>) on the idea. I gave a talk in may 2013 at the PLUME seminar in
Lyon,
whose <a href="doc/unique-inhabitants-slides-plume-2013-scherer.pdf">slides</a>
are available. I discussed some examples applications at the
Dependently Typed Programming (DTP'13) Workshop
(<a href="research/unique_inhabitants/abstract-dtp2013.pdf">abstract</a>, <a href="research/unique_inhabitants/talk-dtp2013.pdf">slides</a>)</p>

<p>
In early 2015 I proposed an algorithm to decide unique inhabitation in
the simply-lambda calculus with sums, modulo
beta-eta-equivalence. This work is described in
a <a href="research/unique_inhabitants/unique_stlc_sums-long.pdf">paper</a>
and a prototype implementation is
available <a href="research/unique_inhabitants/">there</a>.
</p>

<p>My thesis, defended in March 2016, expands on this. See the
specific <a href="http://www.ccs.neu.edu/home/gasche/phd_thesis/">phd
page</a> or
the <a href="http://www.ccs.neu.edu/home/gasche/phd_thesis/scherer-thesis.pdf">manuscript</a>. Abstract:
<blockquote>
<p>
  Some programming language features (coercions, type-classes,
  implicits) rely on inferring a part of the code that is determined
  by its usage context. In order to better understand the theoretical
  underpinnings of this mechanism, we ask: when is it the case that
  there is a <em>unique</em> program that could have been guessed, or
  in other words that all possible guesses result in equivalent
  program fragments? Which types have a unique inhabitant?
</p><p>
  To approach the question of unicity, we build on work in
  proof theory on more canonical representation of
  proofs. Using the proofs-as-programs correspondence, we can
  adapt the logical technique of focusing to obtain more
  canonical program representations.
</p><p>
  In the setting of simply-typed lambda-calculus with sums and the
  empty type, equipped with the strong beta-eta-equivalence, we show
  that uniqueness is decidable. We present a saturating focused logic
  that introduces irreducible cuts on positive types “as soon as
  possible”. Goal-directed proof search in this logic gives an
  effective algorithm that returns either zero, one or two distinct
  inhabitants for any given type.
</p>
</blockquote>
</p>

<h2 id="eta-sums">Eta-equivalence with sums</h2>

<p>To start the research on unique inhabitants, I have been looking at
the work on ((maximal) (multi-)focusing for type/proof systems with
sum types. (See these
2014 <a href="doc/multifocusing-slides-mar-2014-scherer.pdf">slides</a>
introducing maximal multi-focusing.)</p>

<p>There are interesting connections between these proof-theoretical
techniques and the problem of deciding equivalence of pure
program. I have looked at these connections from two different
angles:</p>

<ul>
  <li><p>If multi-focusing is formulated in a logic closer to the
      lambda-calculus (intuitionistic natural deduction), it reveals
      close links with existing work on equivalence algorithms for
      simply-typed lambda-calculus with sums, in particular by Sam
      Lindley. This is described
      in <a href="drafts/multifoc_sums.pdf">this preprint</a>
      (written in 2013, revised in early 2015).</p></li>
  <li><p> Another direction is to bring programming languages closer
      to the elegant symmetries of the sequent calculus. I have
      <a href="https://hal.inria.fr/hal-01160579">collaborated with
      Guillaume Munch-Maccagnoni</a> (2015) on the study of normal
      forms in a calculus of sequent-style terms, polarized System L,
      in presence of sums. This allows to describe algorithms deciding
      beta-eta equivalence in a very uniform way, directly at the
      level of untyped terms.
  </p></li>
</ul>

<h2 id="full-reduction">Full reduction in the face of absurdity</h2>

<p>When designing type systems for programming languages, there is often
an unnoticed mismatch between the dynamic semantics we use and the
soundness guarantees given by the type system. We use <em>weak</em>
reduction strategies (for implementation reasons or to reason about
side-effect order) but type-soundness would actually hold ("Well-typed
program do not go wrong") if we used <em>full</em> reduction, allowing
to reduce under lambdas.</p>

<p>This is all fine, until we end up designing language feature that
break the latter property (safety with respect to <em>full</em>
reduction), and we don't notice because we only play with the weak
reduction. It means there is something fundamental about those
features that we haven't noticed/understood, that should be important
for reasoning about programs. <acronym title="Generalized/Guarded
Algebraic Datatypes">GADTs</acronym> are such a feature.</p>

<p>Didier and I study this question in the following
draft: <a href="http://gallium.inria.fr/~remy/coercions/Scherer-Remy!fth@long2014.pdf">Full
reduction in the face of absurdity</a> (2014). To technically support
our consideration, this document uses
a <em>consistent <a href="http://gallium.inria.fr/~remy/coercions/">coercion</a>
calculus</em> as developped by Julien Crétin during
his <a href="http://phd.ia0.fr/">PhD thesis</a> -- it is a very
convenient and expressive framework in which to study these
issues. For a simplified setting (System F plus logical assumptions),
you may want to look at <a href="drafts/talk-scherer-ltp14.pdf">my
slides (with notes)</a> from the
<a href="https://www.lri.fr/~conchon/LTP2014/">Journées LTP du GDR
GPL</a>, October 2014.</p>


<h2 id="gadts-subtyping">Theoretical aspects of mixing GADTs and subtyping in OCaml</h2>

<p>When GADTs were added by Jacques Garrigue and Jacques Le Normand in
OCaml 4.00, we found out that their interaction with subtyping had not
been studied theoretically, in particular we were not sure how to
soundly assign variances to GADT parameters, and a very restrictive
criterion was implement in OCaml. I worked in 2012 on finding the
right variance check for GADTs, and this resulted in the
article <a href="http://arxiv.org/abs/1301.2903">GADTs meet subtyping
(arxiv)</a>, also available as
a <a href="http://gallium.inria.fr/~remy/gadts/Scherer-Remy:gadts-subtyping@inria2012.pdf">long
version</a>
or <a href="doc/gadts-meet-subtyping-slides-esop-2013-scherer-remy.pdf">slides</a>.</p>

<h2 id="mltt">Meta-theory of predicative dependent type theories, using logical relations</h2>

<p>In 2011, I worked with <a href="http://www2.tcs.ifi.lmu.de/~abel/">Andreas
Abel</a> on meta-theoretical aspects of predicative dependent type
theories -- definitional-algorithmic correspondence with logical
relations, and universe subtyping and polymorphism. I enjoyed
collaborating to his article <a href="http://arxiv.org/abs/1203.4716">
On Irrelevance and Algorithmic Equality in Predicative Type Theory
(arxiv)</a>, and also wrote a much less
rigorous <a href="doc/munich-report-2011-scherer-abel.pdf">internship
report</a> on the question of universe subtyping.</p>

<h4>Computational meaning of semantic proofs techniques</h4>

I find the effectiveness of logical relations rather perplexing. In
recent work (2015)
with <a href="http://pagesperso-systeme.lip6.fr/Pierre-Evariste.Dagand/">Pierre-Évariste
Dagand</a>, I tried to better understand semantic techniques in a much
simpler setting, by studying the computational content of a classical
realizability proof on the simply-typed
lambda-calculus: <a href="http://gallium.inria.fr/~scherer/research/norm_by_rea/dagand-scherer-jfla15.pdf">Normalization
by Realizability also Evaluates</a>.

<h2 id="mlf">MLF type inference and type-level computation</h2>

<p>In a former internship with Didier Rémy at Gallium (2010), I worked
on <a href="http://gallium.inria.fr/~remy/mlf/">ML<sup>F</sup></a>, an
System F-like type system with type inference; I did
some <a href="https://gitorious.org/camlf">prototyping</a> and
explored adding F<sub>ω</sub>-style type-level
operators. A <a href="doc/research-report-mlf-2010-scherer-remy.pdf">horribly-formated
report</a> is available, but this topic would need some more work
because it raised more questions than it answered.</p>

<h2 id="macaque">A type-safe database library using phantom types</h2>

<p>In June-July 2009, I worked
with <a href="http://www.pps.jussieu.fr/~vouillon">Jérôme Vouillon</a>
on a statically typed domain-specific language for database queries
embedded in OCaml, Macaque. The library
is <a href="http://macaque.forge.ocamlcore.org/">publicly
available</a> and in a minimal maintainance mode (if bugs are found,
they get fixed). We wrote
a <a href="doc/macaque-jfla-2010-scherer-vouillon.pdf">french-language
article</a> for JFLA'10.</p>

</body>
</html>

let pp_list_funky li =
  let pp_sep ppf () = Format.fprintf ppf " then " in
  Format.pp_print_list ~pp_sep li;;

#install_printer pp_list_funky;;

[1; 2; 3];;

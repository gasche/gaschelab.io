let hash li =
  let t = Hashtbl.create 10 in
  List.iter (fun (k,v) -> Hashtbl.add t k v) li;
  t

let ( .{} ) = Hashtbl.find
let ( .{}<- ) = Hashtbl.replace

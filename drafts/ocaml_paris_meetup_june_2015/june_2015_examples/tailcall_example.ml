type 'a tree =
  | Node of 'a tree * 'a * 'a tree
  | Leaf

let to_list tree =
  let rec list tree acc = match tree with
    | Leaf -> acc
    | Node (left, x, right) ->
       (list [@tailcall]) left (x :: list right acc)
  in list tree []

open Cute_hash_tables

let test = hash [("foo", 1); ("bar", 2)]
let () =
  print_int test.{"foo"};
  test.{"bar"} <- 3;
  print_int test.{"bar"};
  ()

% Some new things in 4.02
% Gabriel Scherer
% OCaml Paris Meeting, May 22st

## Time machine

I made an OUPS talk on May 21, 2013. Some quotes:

> We already do have a lot of external contributors on the bugtracker!

> Breaking changes are not accepted.

> Bugfixes and contributions for OCamlbuild are desirable and welcome.

> review proposed patches: read the code to give an opinion, improve
> on the implementation, or test them against your own software

## Today (May 22, 2014)

Some new things in 4.02

Please ask questions,

but I don't have all the answers.

## Fresh from the changelog:

> Language features:
>
> - Attributes and extension nodes
>   (Alain Frisch)
> - PR#5095: Generative functors
>   (Jacques Garrigue)
> - Module aliases
>   (Jacques Garrigue)
> - Separation between read-only strings (type string) and read-write byte
>   sequences (type bytes). Activated by command-line option `-safe-string`.
>   (Damien Doligez)
> - PR#6318: Exception cases in pattern matching
>   (Jeremy Yallop, backend by Alain Frisch)
> - PR#5584: Extensible open datatypes
>   (Leo White)
> 
> (Camlp4, Labltk split off the distribution)

## Attributes and extension nodes

General idea: a robust syntax to robustly express a *subset* of Camlp4
extensions. Attach information to AST nodes, or fill new nodes.

> Alternative syntax for string literals {id|...|id} (can break comments)
> (Alain Frisch)

.

    (* find good and bad nodes in {w|u<=w} *)

Examples from `extensions.ml`

In the future: ulex, maybe lwt...

Merlin support?

## Generative functors

> Allow opening a first-class module or applying a generative functor
> in the body of a generative functor. Allow it also in the body of
> an applicative functor if no types are created
> (Jacques Garrigue, suggestion by Leo White)

## Module aliases

(toplevel demo)

##

Use this to avoid module clashes: `mylib_format.ml`, `mylib_url.ml`, and
`mylib.ml` with:

```ocaml
module Format = Mylib_format
module Url = Mylib_url
...
```

Then your users can `open Mylib` and use `Format`, `Url` etc.

## Immutable strings

```ocaml
let reverse str =
  let len = String.length str in
  let res = String.create len in
  for i = 0 to len - 1 do
    res.[i] <- str.[len - 1 - i] (* boo! *)
  done;
  res
```

##

```ocaml
let reverse str =
  let len = String.length str in
  let res = String.create len in
  for i = 0 to len - 1 do
    res.[i] <- str.[len - 1 - i] (* boo! *)
  done;
  res
```

```ocaml
let reverse str =
  let len = String.length str in
  let res = Bytes.create len in
  for i = 0 to len - 1 do
    Bytes.set res i str.[len - 1 - i]
  done;
  Bytes.unsafe_to_string res (* hmm... *)
```

## `match with exception`

```ocaml
let rec read_all () =
  try input_line () :: read_all ()
  with End_of_input -> []
```

##

```ocaml
let rec read_all () =
  try input_line () :: read_all ()
  with End_of_input -> []

let rec read_all acc =
  match input_line () with
  | line -> read_all (line :: acc)
  | exception End_of_input -> List.rev acc

let read_all () = read_all []
```

Even better than `let try`! ("Exceptional Syntax", Nick Benton and Andrew Kennedy, 2001)

## Extensible sum types

An extension of exceptions (add new constructors).

```ocaml
type 'a foo = ..

type 'a foo +=
    A of 'a list
  | B: int foo
```

You will probably never need it, but someone in a far galaxy...

("Polymorphic typed defunctionalized", François Pottier and Nadji Gauthier)

## Other things

- PR#5779: better sharing of structured constants
  (Alain Frisch)

- PR#5899: a programmer-friendly access to backtrace information
  (Jacques-Henri Jourdan and Gabriel Scherer)

- PR#5935: a faster version of "raise" which does not maintain the backtrace
  (Alain Frisch)

- PR#6017: a new format implementation based on GADTs
  (Benoît Vaugon and Gabriel Scherer)

##

- PR#6054: add support for M.[ foo ], M.[| foo |] etc.
  (Kaustuv Chaudhuri)

- PR#6064: GADT representation for Bigarray.kind + CAML_BA_CHAR runtime kind
  (Jeremy Yallop, review by Gabriel Scherer)

- PR#6071: Add a -noinit option to the toplevel
  (David Sheets)

- PR#6180: efficient creation of uninitialized float arrays
  (Alain Frisch, request by Markus Mottl)

##

- PR#6257: handle full doc comments for variant constructors and
  record fields
  (Maxence Guesdon, request by ygrek)

- PR#6270: remove need for -I directives to ocamldebug in common case
  (Josh Watzman, review by Xavier Clerc and Alain Frisch)

- PR#6274: allow doc comments on object types
  (Thomas Refis)

- ocamllex: user-definable refill action
  (Frédéric Bour, review by Gabriel Scherer and Luc Maranget)

- `-no-naked-pointers` (Mark Shinwell, reviews by Damien Doligez and
  Xavier Leroy)

##

- Fixed a major performance problem on large heaps (~1GB) by making heap
  increments proportional to heap size by default
  (Damien Doligez)

- New back-end optimization pass: common subexpression elimination (CSE).
  (Reuses results of previous computations instead of recomputing them.)
  (Xavier Leroy)

- New back-end optimization pass: dead code elimination.
  (Removes arithmetic and load instructions whose results are unused.)
  (Xavier Leroy)

## Thanks. Questions?


<!-- pandoc -s --mathml -i -t dzslides ocaml-paris-meeting-may-2014-scherer.text -o ocaml-paris-meeting-may-2014-scherer.html -->

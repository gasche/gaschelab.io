# Sujet

## Exercice 1

Implémenter la fonction suivante:

    ; same-first-and-second-letter? : String -> Bool
    ; are the first and second letter of the input the same?
    (check-expect (same-first-and-second-letter? "yacine") #false)
    (check-expect (same-first-and-second-letter? "lloris") #true)

### Corrigé

    ; same-first-and-second-letter? : String -> Bool
    ; are the first and second letter of the input the same?
    (check-expect (same-first-and-second-letter? "yacine") #false)
    (check-expect (same-first-and-second-letter? "lloris") #true)
    (define (same-first-and-second-letter? str)
      (and
       (>= (string-length str) 2)
       (string=? (string-ith str 0) (string-ith str 1))))

## Exercice 2

On définit deux structures de formes géométriques ("shapes") avec des couleurs.

    ; height: Number (hauteur)
    ; width: Number (largeur)
    ; color: String
    (define-struct rect-shape [height width color])

    ; radius: Number (rayon)
    ; color: String
    (define-struct circle-shape [radius color])

On appelle `Shape` un valeur qui est soit un `rect-shape` soit un `circle-shape`

    ; A Shape is one of the following:
    ; - a rect-shape
    ; - a circle-shape

Vous devez implémenter les deux fonctions suivantes:

    ; colors are represented as String
    ; change-color: Shape -> String -> Shape
    ; takes a shape and a color, and returns a modified shape of that color
    (check-expect
      (change-color (make-rect-shape 10 20 "blue") "red")
      (make-rect-shape 10 20 "red"))
    (check-expect
      (change-color (make-circle-shape 10 "blue") "green")
      (make-circle-shape 10 "green"))

et:

    (require 2htdp/image)
    ; draw-shape: Shape -> Image
    ; converts a shape into an image
    (check-expect
      (draw-shape (make-rect-shape 10 20 "blue"))
      (rectangle 10 20 "solid" "blue"))
    (check-expect
      (draw-shape (make-circle-shape 10 "blue"))
      (circle 10 "solid" "blue"))

### Corrigé

    ; height: Number (hauteur)
    ; width: Number (largeur)
    ; color: String
    (define-struct rect-shape [height width color])
    
    ; radius: Number (rayon)
    ; color: String
    (define-struct circle-shape [radius color])
    
    ; A Shape is one of the following:
    ; - a rect-shape
    ; - a circle-shape
    
    ; colors are represented as String
    ; change-color: Shape -> String -> Shape
    ; takes a shape and a color, and returns a modified shape of that color
    (check-expect
      (change-color (make-rect-shape 10 20 "blue") "red")
      (make-rect-shape 10 20 "red"))
    (check-expect
      (change-color (make-circle-shape 10 "blue") "green")
      (make-circle-shape 10 "green"))
    (define (change-color shape color)
      (cond
        [(rect-shape? shape)
         (make-rect-shape (rect-shape-height shape) (rect-shape-width shape) color)]
        [(circle-shape? shape)
         (make-circle-shape (circle-shape-radius shape) color)]))
    
    (require 2htdp/image)
    ; draw-shape: Shape -> Image
    ; converts a shape into an image
    (check-expect
      (draw-shape (make-rect-shape 10 20 "blue"))
      (rectangle 10 20 "solid" "blue"))
    (check-expect
      (draw-shape (make-circle-shape 10 "blue"))
      (circle 10 "solid" "blue"))
    (define (draw-shape shape)
      (cond
        [(rect-shape? shape)
         (rectangle
          (rect-shape-height shape)
          (rect-shape-width shape)
          "solid"
          (rect-shape-color shape))]
        [(circle-shape? shape)
         (circle
          (circle-shape-radius shape)
          "solid"
          (circle-shape-color shape))]))

## Exercice 3

    ; all-strictly-positive? : List Number -> Bool
    ; all-strictly-positive? checks that all numbers in a list
    ; are strictly positive? (> n 0)
    (check-expect (all-strictly-positive? (cons 1 '())) #true)
    (check-expect (all-strictly-positive? (cons 2 (cons -1 '()))) #false)

### Corrigé

    ; all-strictly-positive? : List Number -> Bool
    ; all-strictly-positive? checks that all numbers in a list
    ; are strictly positive? (> n 0)
    (check-expect (all-strictly-positive? (cons 1 '())) #true)
    (check-expect (all-strictly-positive? (cons 2 (cons -1 '()))) #false)
    (define (all-strictly-positive? li)
      (cond
        [(empty? li) #true]
        [(cons? li) (and (> (first li) 0) (all-strictly-positive? (rest li)))]))

## Exercice bonus

Suivre la recette de conception pour implémenter une fonction
`maxi` qui prend deux nombres et renvoie le plus grand des deux.

### Corrigé

    ; maxi: Number Number -> Number
    ; prend deux nombres et renvoie le plus grand des deux
    (check-expect (maxi 1 2) 2)
    (check-expect (maxi -3 3) 3)
    (check-expect (maxi 4 4) 4)
    (define (maxi a b) (if (> a b) a b))

# Rappels

## Syntaxe

Expressions:

    (opération argument1 argument2 ...)

Définition et utilisation de variable:

    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))

    (square 5)

## Opérations courantes

    (if truc machin chouette)
    (cond
      [truc bidule]
      [truc machine]
      [else chose])

nombres

    + - * /

images

    (require 2htdp/image)
    (rectangle 10 20 "solid" "blue")
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string->number number->string
    string=?

booléens

    and or not

prédicats de test

    image? number? string? boolean?

structures

    (define-struct anim [size decr?])

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

listes

    '() cons first rest empty? cons?

## Recette de conception

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

Exemple:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three
    ; possible states that a traffic light may assume

    ; the state of a traffic light is represented by TrafficLight
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

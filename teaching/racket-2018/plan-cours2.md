# Rappel sur le cours précédent,

- principe: survol d'ensemble
- calculs simples
- principes de programmation
- tests
- images
- animations

Questions ?

# Livre du cours

## Présentation du livre HTDP

<https://htdp.org/2018-01-06/Book/>

## Projet: suivre le cours du livre

- je choisis quoi présenter par chapitre
- en TPs, on fait une partie des exercices
- 3 TPs noté
- si possible, un projet de programmation (à la maison)

# Cours d'ajourd'hui:

<https://htdp.org/2018-01-06/Book/part_one.html#(part._ch~3abasic-arithmetic)>

Calcul sur des données atomiques

- nombres (déjà vu)
- chaînes de caractères
- booléens
- images (déjà un peu vu)

 Façon de calculer.

## Nombres

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aarith-num)>

    (+ 1 2 3)

    (* 4 3 (/ 6 2))

## Chaînes

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aarith-str)>

    string-append, string-length

    (+ (string-length (number->string 42)) 2)
    (+ (string-length 42) 1)

Exercice: ajouter un espace entre deux chaînes.

(On fait certains exercices ensemble, on va les refaire en TP.)

## Images

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aarith-images)>

(déjà vu la dernière fois)

ouvrir un nouvel onglet, charger 2hdtp/image

montrer que l'on peut couper-coller des images

image-width, image-height

Exercice: nombre de pixels d'une image

## Booléens

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aarith-bools)>

    #true, #false

    and, or, not

Exercice 7:

    (define nice-weather ...)
    (define friday ...)
    (define good-mood (and nice-weather friday))

## Mélanges

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aboolean-if)>

     (define x 2)
     (1 / x)

     (define (divide-or-zero x)
        (if (= x 0) 0 (/ 1 x)))

Exécuter, voir les zones noires.

    (divide-or-zero 2)
    (divide-or-zero 0)

Stepper.

## Tester ses données

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3apredicates)>

    number?
    string?
    image?
    boolean?

Facultatif:

    integer?
    exact?
    inexact?

Fin des deux heures ?

# TP

Exercise 16. Define the function image-area, which counts the number
  of pixels in a given image. See exercise 6 for ideas.

Exercise 17. Define the function image-classify, which consumes an
  image and conditionally produces "tall" if the image is taller than
  wide, "wide" if it is wider than tall, or "square" if its width and
  height are the same. See exercise 8 for ideas.

# Rappel sur le cours précédent

## Comment concevoir des programmes:

- entrées, sorties, relation ?
- décomposition en fonctions
- fonctions déjà existantes ?

## Comment concevoir des fonctions: recette de conception

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3adesign-func)>

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

## Structures

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3astructures)>

    (define-struct anim [size decr?])
    
    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)


# Section 3.5: Tests

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3atesting)>

Schéma de test:

    (check-expect (your-function inputs) output)

Exemple:

    ; Number -> Number
    ; converts a number of euros into the equivalent amount of dollars
    (check-expect (euros->dollars 1) 1.14)
    (check-expect (euros->dollars 10) 11.4)
    (check-expect (euros->dollars 100) 114)
    (define (euros->dollars x) (* x 1.14))

# Chapitre 4: conditions, énumérations

<https://htdp.org/2018-01-06/Book/part_one.html#(part._ch~3aintervals-enums)>

## `cond`

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aworks)>

    (define (reward s)
      (cond
        [(<= 0 s 10) "bronze"]
        [(and (< 10 s) (<= s 20)) "silver"]
        [else "gold"]))


## Enumerations

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aenums)>

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three 
    ; possible states that a traffic light may assume 
    
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

## Unions

<https://htdp.org/2018-01-06/Book/part_one.html#(part._itemization._sec~3aitemization)>

    ; An NorF is one of: 
    ; – #false
    ; – a Number

On a déjà vu une fonction utilisant cette signature: `string->number`

    (check-expect (string-number "3") 3)
    (check-expect (string-number "toto") #false)

Exercice:

    ; sumF : NorF -> NorF -> NorF
    ; sums two NorF, but returns #false if either one is false
    (check-expect (sumF 1 2) 3)
    (check-expect (sumF 1 #false) #false)
    (check-expect (sumF #false 2) #false)
    (check-expect (sumF #false #false) #false)


Si on a du temps en plus:

# Chapitre 8: listes

<https://htdp.org/2018-01-06/Book/part_two.html#(part._ch~3alists1)>

    '()
    (cons hd tl)
    
    '()
    (cons "Mars" '())
    (cons "Venus" (cons "Mars" '()))
    (cons "Mercure" (cons "Venue" (cons "Mars" '())))


    ; A List-of-names is one of: 
    ; – '()
    ; – (cons String List-of-names)
    ; interpretation a list of students, by first name


    ; empty? : Any -> Bool
    ; is the input the empty list ?
    (empty? x)
    
    ; is the input a (cons a b)?
    (cons? x)
    
    ; first : (cons Any List) -> Any
    ; (first (cons a b)) is a
    (first x)
    
    ; rest : (cons Any List) -> List
    ; (rest (cons a b)) is b
    (rest x)

Exercice sur les listes de noms:

    ; contains-Gabriel? : List-of-names -> Bool
    (check-expect (contains-Gabriel? '()) #false)
    (check-expect (contains-Gabriel? (cons "Gabriel" '())) #true)
    (check-expect (contains-Gabriel? (cons "Sarah" '())) #false)
    (check-expect (contains-Gabriel? (cons "Zachary" (cons "Gabriel " '()))) #true)
    
    (define (contains-Gabriel? names)
      (cond
        [(empty? names) ...]
        [(cons? names) ... (first names) ... (rest names) ... ]))

Corrigé:

    (define (contains-Gabriel? names)
      (cond
        [(empty? names) #false]
        [(cons? names)
           (or (string=? "Gabriel" (first names))
               (contains-Gabriel? (rest names)))]))

Notes de discussion pendant le TP:
  Envoyer aux élèves:
  - les corrigés du TP noté
  - des exercices sur le cours des deux dernières semaines
  - mettre les plans de cours sur internet

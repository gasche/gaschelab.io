(question de calendrier)

# Rappel sur le cours précédent

Type de données de base / atomiques

- nombres

        + - * /

- images

        (circle 100 "solid" "red") square star
        above beside
        image-width image-height

- chaînes de caractères (strings)

        string-length string-append string-ith
        number->string

- booléens

        and or not

- prédicats de test

        image? number? string? boolean?

Point sur le déroulement du cours et du TP.

Distribution de la feuille de rappels / TP.

## Rappel de syntaxe

Expressions:
  
    (opération argument1 argument2 ...)
  
Définition et utilisation de variable:
  
    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))  

    gregory

Définition et utilisation de fonction:
  
    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))
  
    (square 5)

# Chapitre 2: Fonctions et programmes

<https://htdp.org/2018-01-06/Book/part_one.html#(part._ch~3afuncs-progs)>

Exercices:

    (string-first str)
    (string-last str)
    (count-pixels img)
    (string-delete str i)

## Section 2.3 : Composition de fonctions

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3acomposing)>

Sortie souhaitée:

    "Bonjour Yacine,
    
    De façon exceptionnelle, nous envisageons d'ouvrir
    l'université dimanche prochain. Serais-tu partant
    pour venir en cours ?
    
    Bon courage,
    Gabriel"

Programme découpé en plusieurs fonctions:

    (define (letter receiver sender)
      (string-append
        (opening receiver)
        "\n\n"
        body
        "\n\n"
        (closing sender)))
    
    (define (opening name)
       (string-append "Bonjour " name ","))
    
    (define body
            (string-append
                    "De façon exceptionnelle, nous envisageons d'ouvrir\n"
                    "l'université dimanche prochain. Serais-tu partant\n"
                    "pour venir en cours ?"))
    
    (define (closing sender)
      (string-append "Bon courage,\n" sender))

Écrire dans un fichier ou dans la sortie standard:

    (require 2htdp/batch-io)
    (write-file 'stdout (letter "Yacine" "Gabriel"))

## Section 2.5: Programmes

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3aprogs)>

Programme "batch" (de calcul) vs. programme interactif (événements)

### Programme batch / programme de calcul

Se lance, lit des données, fait un calcul, écrit des données,
s'arrête. (Parfoit il lit et écrit en même temps)

    (define (euro->dollar x) ...)
    
    (define (convert in out)
      (write-file out
        (string-append
          (number->string
            (euro->dollar
              (string->number
                (read-file in))))
          "\n")))
    
    (convert "entree.txt" "sortie.txt")
    (convert "entree.txt" 'stdout)
    (convert 'stdin 'stdout)

### Programme interactif (Application ?)

Interaction avec une partie du monde extérieur.

Réagit à des événements.

Appuyer sur une touche, cliquer, glisser du doigt : évènement.

Fonctions appelées: gestionnaires d'événements.

Gestionnaire d'événements:

- produisent des actions en retour
- mettent à jour l'état du programme

Exemple:

    (define (number->square s)
      (square (* 5 s) "solid" "red"))
    
    (number->square 2)
    (number->square 20)
    (number->square 100)

    (big-bang 100 [to-draw number->square])
    (big-bang 100
        [to-draw number->square]
        [on-tick sub1]
        [stop-when zero?])

Version qui recommence à zéro quand on appuie sur une touche:

    (big-bang 100
        [to-draw number->square]
        [on-tick sub1]
        [stop-when zero?]
        [on-key reset])
    
    (define (reset state key) 100)

Schema général:

    (big-bang start-state
      [on-tick tock]
      [on-key key-handler]
      [on-mouse mouse-handler]
      [to-draw render]
      [stop-when end?]
      ...)

Réalisation en cours:

    (require 2htdp/image)
    (require 2htdp/universe)
    
    (define (image n)
      (circle
       (if (>= n 0) n (- n))
       "solid" "red"))
    
    (define (next n)
      (if (>= n 0)
          (- n 1)
          (if (= n -100) 99 (- n 1))))
    
    (define start-state 100)
    (define (key-handler n evt)
      start-state)
    
    (big-bang start-state
        [to-draw image]
        [on-tick next]
        [on-key key-handler])

# Exercices d'entraînement

## Fonctions

Définir une fonction `(and3 a b c)` qui renvoie `#true` si tous les
booléens `a, b, c` sont vrais, et `#false` sinon, sans utiliser la
fonction `and`.

Définir une fonction `(max2 a b)` qui prend deux nombres `a, b`
et renvoie le plus grand, sans utiliser la fonction `max`.

Définir une fonction `(max3 a b c)` qui prend trois nombres `a, b, c`
et renvoie le plus grand, sans utiliser la fonction `max`.

## Structures

Définir une structure `adresse` avec un champ `numéro`, un champ `rue`
et un champ `ville`.

Définir une fonction `adresse->string` qui renvoie la représentation
textuelle d'une adresse.

Par exemple, si l'adresse est
- numéro: `2`
- rue: `"rue de la Liberté"`
- ville: `"Saint-Denis"`
le résultat attendu est `"2 rue de la Liberté, Saint-Denis"`.

Écrire la fonction `adresse-suivante` qui renvoie l'adresse suivante
de la rue. Avec l'exemple précédent, on attend le numéro `3`, rue
`"rue de la Liberté"`, ville `"Saint-Denis"`.

## Unions

On définit un nombre-étendu comme soit:

- un nombre, ou
- un booléen: `#true` représente "plus l'infini", `#false` représente "moins l'infini"

Écrire une fonction `add` qui prend deux nombres-étendus et les ajoutes:

- un nombre plus "plus l'infini", c'est "plus l'infini"
- un nombre plus "moins l'infini", c'est "moins l'infini"
- "plus l'infini" plus "plus l'infini", c'est "plus l'infini"
- pareil pour "moins l'infini"
- si on ajoute "plus l'infini" et "moins l'infini", vous pouvez renvoyer ce que vous voulez.

Par exemple, `(add 3 #true)` (ajouter 3 et "plus l'infini") renvoie `#true`.

## Types récursifs, valeurs récursives

On va définir des types de données pour représenter une suite de
commandes envoyées à un petit robot.

On définit un `mouvement` comme:
- soit le texte `"avance"`
- soit le texte `"recule"`

On définit la structure `action`, avec les champs `dabord` et
`ensuite`.

On définit le type `commande` comme soit:
- la chaîne "stop"
- une `action` dont le champ `dabord` vaut soit `"avance"` soit `"recule"`,
  et le champ `ensuite` est encore une commande.

Question 1: écrire la valeur correspondant à la commande
`"avance. avance. recule. stop."`.

Question 2: écrire une fonction qui compte le nombre de mouvements
présents dans une commande. ("stop" n'est pas un mouvement). Sur
l'exemple précédent, elle renvoie `3`.

Question 3: si on considère que le robot avance de un mètre (+1) quand on
lui demande `"avance"`, et recule de un mètre (-1) quand on lui demande
`"recule"`, écrire une fonction qui prend une commande et renvoie la
position finale, en mètres; pour `"avance. avance. recule. stop."`, on
attend 2, pour `"recule. stop."`, on attend -1.

Question 4: écrire une fonction `Commande -> String` qui prend une
commande et renvoie une description textuelle de la commande, dans le
format que j'utilise dans cet énoncé (avec des points après chaque
mouvement, et après `"stop"`.). Sur l'exemple de départ, on attend la
chaîne `"avance. avance. recule. stop."`.

# Rappels

## Syntaxe

Expressions:

    (opération argument1 argument2 argument3)

Définition et utilisation de variable:

    (define nom-de-variable ...)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 nom-argument-3) ...)
    (define (square i) (* i i))

    (square 5)

## Opérations courantes

    (if truc machin chouette)

    (cond
      [truc bidule]
      [truc machine]
      [else chose])

nombres

    + - * /

images

    (require 2htdp/image)
    (rectangle 10 20 "solid" "blue")
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string->number number->string
    string=?

booléens

    and or not

prédicats de test

    image? number? string? boolean?

structures

    (define-struct anim [size decr?])

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

listes

    '() cons first rest empty? cons?

## Recette de conception

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

Exemple:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three
    ; possible states that a traffic light may assume

    ; the state of a traffic light is represented by TrafficLight
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

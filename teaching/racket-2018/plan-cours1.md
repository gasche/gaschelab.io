Présentation, aime bien les questions.

# Pourquoi vous faites de l'informatique ?

Point commun: savoir dire quoi faire à un ordinateur.

Ordinateur très différent d'un humain: fais très bien les calculs,
  mais ne comprend pas l'imprécis ("très bête").

- des choses qui semblent difficiles sont en fait faciles  
  (exemple: chercher dans tout l'ordinateur un fichier du nom "A")

- des choses qui semblent faciles sont en fait difficiles  
  (les clients, c'est chiant !)  
  (exemple: sauvegarder/relancer l'état d'une application)  
  (exemple: envoyer un email à Toto avec le contenu Pouet)

# Les programmes sont des objets très complexes

  (les plus complex gérés par les humains ?)

  (noyau Linux: 12M lignes; Tour Montparnasse: 7200 vitres. Tour Eiffel: 2.5M rivets. )

  => erreurs, problème de maintenance, travail en équipe

  Ce qu'on va apprendre: faire des programmes
  - qui font ce qu'on veut
  - qui sont clairs et faciles à comprendre et modifier
  - et quand on se trompe, comment les corriger

# Ce qu'on va apprendre

Pour ce cours, pas besoin de savoir programmer

(si vous avez déjà appris, vous pouvez oublier, c'est un langage différent)

On va apprendre à:
- étant donné un problème simple,
  concevoir et implémenter un programme qui le résoud,
  en suivant de bonnes pratiques
- exécuter les programmes dans sa tête pour comprendre comment ils marchent
  (et où se trouvent les erreurs à corriger)

# Commencer

    1*1 + 2*2 + 3*3 + 4*4 + 5*5

    (+ (* 1 1) (* 2 2) (* 3 3) (* 4 4) (* 5 5))

    (define (carre x) (* x x))

    (+ (carre 1) (carre 2) (carre 3) (carre 4) (carre 5))

    (define (somme n)
      (if (= n 0) 0
        (+ (carre n) (somme (n - 1)))))

    (somme 5)

Exemple de debug:

    (define (carre x) (* x x))

    (define (somme n)
      (if (= n 0) 0
        (+ (carre n) (somme n))))

faire tourner le programme par réduction successive  
  => on voit le problème

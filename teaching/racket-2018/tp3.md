# Rappels de syntaxe

Expressions:

    (opération argument1 argument2 ...)

Définition et utilisation de variable:

    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))

    (square 5)

## Rappels d'opérations courantes

nombres

```
+ - * /
```

images

```
(require 2htdp/image)
(circle 100 "solid" "red") square star
above beside
image-width image-height
```

chaînes de caractères (strings)

```
string-length string-append string-ith
string=? string->number number->string
```

booléens

```
and or not
```

prédicats de test

```
image? number? string? boolean?
```

# 1. Révisions sur les fonctions

Écrire et tester les fonctions suivantes:

- `(string-first str)`, donne le premier caractère d'une chaîne
- `(string-last str)`, donne le dernier caractère d'une chaîne
- `(count-pixels img)`, donne le nombre de pixels d'une image
- `(string-delete str i)`, retire le caractère de position `i` de la chaîne `str`

# 2. Programme batch

Rappel de fonctions utiles :

```
(require 2htdp/batch-io)

(write-file filename content)
(write-file "test.txt" "blabla")
(write-file 'stdout    "truc")

(read-file filename)
(read-file "test.txt")
(read-file 'stdin)
```

1. Écrire et tester une fonction `(convert-into-dollars in out)` qui lit un
nombre d'euros dans le fichier `in`, et écrit le montant correspondant
en dollars dans le fichier `out`.

2. Écrire et tester une fonction `(convert-into-euros in out)` qui lit un nombre
de dollars dans le fichier `in`, et écrit le montant correspondant en
euros dans le fichier `out`.

3. Écrire et tester une fonction `(convert f in out)` qui lit un
nombre dans le fichier `in`, applique la fonction `f` dessus pour
obtenir un nouveau nombre, et écrit le montant correspondant en euros
dans le fichier `out`. Redéfinir et tester `convert-into-dollars` et
`convert-into-euros` à partir de `convert`.


# 3. Programme interactif

```racket
(require 2htdp/image)
(require 2htdp/universe)

(define (image n) ?)

(define (next n) ?)

(define start-state 100)

(define (key-handler n evt) start-state)

(big-bang start-state
    [to-draw image]
    [on-tick next]
    [on-key key-handler])
```

Remplir le code ci-dessus avec le code nécessaire pour avoir une
animation qui descend jusqu'à 0, et repart de l'état de départ
quand on atteint 0.



# Corrigés

## Révisions sur les fonctions

```racket
(define (string-first str) (string-ith str 0))

(define (string-last str)
  (string-ith str (- (string-length str) 1)))

(require 2htdp/image)
(define (count-pixels img)
  (* (image-width img) (image-height img)))

(define (string-delete str i)
  (string-append (substring str 0 i) (substring (+ i 1))))
```


## Programme batch

```racket
(define (convert f in out)
  (write-file out
    (number->string
      (f
        (string->number
          (read-file in))))))
```

## Programme interactif

```racket
(require 2htdp/image)
(require 2htdp/universe)

(define (image n) (circle n "solid" "red"))

(define (next n) (if (= n 0) start-state (- n 1)))

(define start-state 100)

(define (key-handler n evt) start-state)

(big-bang start-state
    [to-draw image]
    [on-tick next]
    [on-key key-handler])
```

# Rappels de syntaxe

Expressions:

    (opération argument1 argument2 ...)

Définition et utilisation de variable:

    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))

    (square 5)

## Rappels d'opérations courantes

nombres

    + - * /


images

    (require 2htdp/image)
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string=? string->number number->string

booléens

    and or not

prédicats de test

    image? number? string? boolean?


# TP5

Compléter le programme suivant.

    (require 2htdp/image)
    (require 2htdp/universe)

    ; TrafficLight is one of
    ; - "green"
    ; - "orange"
    ; - "red"

    ; next-light: TrafficLight -> TrafficLight
    ; returns the next state of a traffic light
    (check-expect (next-light "green") "orange")
    (check-expect (next-light "red") "green")
    ; TODO: implement this program

    ; Lamp is one of
    ; - TrafficLight
    ; - #false

    (define LIGHT_SIZE 20)
    ; lamp-draw: Lamp -> Image
    (check-expect (lamp-draw "red") (circle LIGHT_SIZE "solid" "red"))
    (check-expect (lamp-draw #false) (circle LIGHT_SIZE "solid" "black"))
    ; TODO: implement this program

    ; switch: TrafficLight -> Lamp -> Lamp
    ; switch the Lamp to #false if it is not in the same state as the TrafficLight
    (check-expect (switch "green" "green") "green")
    (check-expect (switch "green" "red") #false)
    ; TODO: implement this program

    ; traffic-light-draw: TrafficLight -> Image
    ; TODO: implement this program

    ; key-handler: TrafficLight -> KeyEvent -> TrafficLight
    ; on any key, move to the next traffic light
    (check-expect (key-handler "orange" "anything") "red")
    ; TODO: implement this program

    ; main program
    (big-bang "green"
      [to-draw traffic-light-draw]
      [on-key key-handler])

----

# TP5: corrigé

```racket
(require 2htdp/image)
(require 2htdp/universe)

; TrafficLight is one of
; - "green"
; - "orange"
; - "red"

; next-light: TrafficLight -> TrafficLight
; returns the next state of a traffic light
(check-expect (next-light "green") "orange")
(check-expect (next-light "red") "green")
(define (next-light l)
  (cond
    [(string=? l "green") "orange"]
    [(string=? l "orange") "red"]
    [(string=? l "red") "green"]))

; Lamp is one of
; - TrafficLight
; - #false

(define LIGHT_SIZE 20)
; lamp-draw: Lamp -> Image
(check-expect (lamp-draw "red") (circle LIGHT_SIZE "solid" "red"))
(check-expect (lamp-draw #false) (circle LIGHT_SIZE "solid" "black"))
(define (lamp-draw lamp)
  (circle LIGHT_SIZE "solid" (if (false? lamp) "black" lamp)))

; switch: TrafficLight -> Lamp -> Lamp
; switch the Lamp to #false if it is not in the same state as the TrafficLight
(check-expect (switch "green" "green") "green")
(check-expect (switch "green" "red") #false)
(define (switch light lamp)
  (cond
    [(false? lamp) lamp]
    [(string=? lamp light) lamp]
    [else #false]))

; traffic-light-draw: TrafficLight -> Image
(define (traffic-light-draw light)
  (above
   (lamp-draw (switch light "red"))
   (lamp-draw (switch light "orange"))
   (lamp-draw (switch light "green"))))

; key-handler: TrafficLight -> KeyEvent -> TrafficLight
; on any key, move to the next traffic light
(check-expect (key-handler "orange" "anything") "red")
(define (key-handler light evt) (next-light light))

; main program
(big-bang "green"
  [to-draw traffic-light-draw]
  [on-key key-handler])
```

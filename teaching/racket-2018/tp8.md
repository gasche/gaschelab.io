# Sujet (avec corrigé)

## Fonctions

Définir une fonction `(min2 a b)` qui prend deux nombres `a, b` et
renvoie le plus petit, sans utiliser la fonction `min`.

    ; min2: Number Number -> Number
    (check-expect (min2 1 3) 1)
    (check-expect (min2 5 -1) -1)
    (define (min2 a b) (if (< a b) a b))
    
Définir une fonction `(min3 a b c)` qui prend trois nombres `a, b, c`
et renvoie le plus petit, sans utiliser la fonction `min` -- mais on
pourra utiliser `min2` définie ci-dessus

    ; min3: Number Number Number -> Number
    (check-expect (min3 1 3 2) 1)
    (check-expect (min3 5 7 -1) -1)
    (define (min3 a b c) (min2 a (min2 b c)))

## Structures

Définir une structure `date` avec un champ `année`, un champ `mois` et
un champ `jour` -- les trois champs sont représentés par des nombres.

    (define-struct date [année mois jour])

Écrire le code qui représente la date du 23 novembre 2018.

    (define important (make-date 2018 11 23))

Définir la fonction `année-suivante` qui prend une date et renvoie la
même date, l'année suivante. Pour le 23 novembre 2018, elle renvoie le
23 novembre 2019.

    (check-expect (année-suivante important) (make-date 2019 11 23))
    (define (année-suivante date)
      (make-date
       (+ 1 (date-année date))
       (date-mois date)
       (date-jour date)))

Définir la fonction `date->string` qui renvoie la date en format
`"jour/mois/année"`; par exemple, pour le 23 novembre 2018, on attend
le résultat `"23/11/2018"`. (Utiliser `string-append` et `number->string`.)    
    
    (check-expect (date->string important) "23/11/2018")
    (define (date->string date)
      (string-append
       (number->string (date-jour date))
       "/"
       (number->string (date-mois date))
       "/"
       (number->string (date-année date))))

## Recette de conception

Suivre la recette de conception pour la fonction `min3` du premier
exercice -- je m'attends à voir chaque étape en commentaire, avec le
code de la fonction à la fin.

    ; min3 : Number Number Number -> Number
    ; returns the smallest of the three numbers given
    (check-expect (min3 1 -3 -5) -5)
    (check-expect (min3 0 1 2) 0)
    (check-expect (min3 2 1 2) 1)
    (define (min3 a b c) (min2 a (min2 b c)))

# Rappels

## Syntaxe

Expressions:

    (opération argument1 argument2 argument3)

Définition et utilisation de variable:

    (define nom-de-variable ...)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 nom-argument-3) ...)
    (define (square i) (* i i))

    (square 5)

## Opérations courantes

    (if truc machin chouette)

    (cond
      [truc bidule]
      [truc machine]
      [else chose])

nombres

    + - * /

images

    (require 2htdp/image)
    (rectangle 10 20 "solid" "blue")
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string->number number->string
    string=?

booléens

    and or not

prédicats de test

    image? number? string? boolean?

structures

    (define-struct anim [size decr?])

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

listes

    '() cons first rest empty? cons?

## Recette de conception

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

Exemple:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three
    ; possible states that a traffic light may assume

    ; the state of a traffic light is represented by TrafficLight
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

# Rappel sur le cours précédent

## Tests

    (check-expect (euros->dollars 1) 1.14)
    (check-expect (euros->dollars 10) 11.4)

## Énumérations

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "orange"
    ; interpretation: the three strings represent the three 
    ; possible states that a traffic light may assume 
    
    ; traffic-light-next: TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "orange"]
        [(string=? "orange" s) "red"]))

## Unions

    ; An NorF is one of: 
    ; – #false
    ; – a Number

    (check-expect (string->number "3") 3)
    (check-expect (string->number "toto") #false)

## Listes

    '()
    (cons hd tl)

    ; A List-of-names is one of: 
    ; – '()
    ; – (cons String List-of-names)
    ; interpretation a list of students, by first name

    ; empty? : Any -> Bool
    ; is the input the empty list ?
    (empty? x)
    
    ; is the input a (cons a b)?
    (cons? x)
    
    ; first : (cons Any List) -> Any
    ; (first (cons a b)) is a
    (first x)
    
    ; rest : (cons Any List) -> List
    ; (rest (cons a b)) is b
    (rest x)

    ; has-Nouh? : List-of-names -> Bool


# Cours

## Conception de fonctions sur les énumérations et unions

<https://htdp.org/2018-01-06/Book/part_one.html#%28part._sec~3adesign-itemization%29>

Quand on prend une entrée qui est définit par une liste de cas
possibles, on peut toujours commencer son code en testant chaque cas.

Énumération:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation: the three strings represent the three 
    ; possible states that a traffic light may assume 
    
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) ?]
        [(string=? "green" s) ?]
        [(string=? "yellow" s) ?]))

Union:

    ; An NorF is one of: 
    ; – a Number
    ; – #false

    ; negate: NorF -> NorF
    (define (negate x)
      (cond
        [(number? x) ?]
        [(false? x) ?]))

## Conception des types récursifs

<https://htdp.org/2018-01-06/Book/part_two.html#%28part._ch~3adesign-lists%29>

"récursif": auto-référent, se mentionne lui-même / elle-même

    ; a List-of-names is one of: 
    ; – '()
    ; – (cons String List-of-names)
    ; interpretation: a list of students, by first name

    ; a List-of-grades is one of: 
    ; – '()
    ; – (cons Number List-of-grades)
    ; interpretation: a list of school grades

Cas de base (pas d'auto-référence), cas récursif (auto-référence).

    ; a ChoiceGame is one of:
    ; - Bool: a result
    ; - (make-choice String ChoiceGame ChoiceGame): a choice

    ; - question: String
    ; - yes: ChoiceGame, what to do if the answer is "yes"
    ; - no: ChoiceGame, what to do if the anser is "no"
    (define-struct choice [question yes no])

## Conception de fonctions sur des types récursifs

<https://htdp.org/2018-01-06/Book/part_two.html#(part._ch~3adesign-lists)>

     ; min: Number Number -> Number

     ; shortest-game: ChoiceGame -> Number

Dans les cas récursifs, on fait des appels récursifs.

    (define (shortest-game game)
      (cond
        [(boolean? game) ...]
        [(choice? game)
          ... (shortest-game (choice-yes game))
          ... (shortest-game (choice-no game))]))

Exercices

     ; sum : list-of-Numbers -> Number
     ; list-length : list-of-Any -> Number
     ; average : list-of-Numbers -> Number

## Récursivité terminale

(hors livre, raccord avec les autres cours)

    ; sum: list-of-Numbers -> Number
    (define (sum li)
      (cond
        [(empty? li) 0]
        [(cons? li) (+ (first li) (sum (rest li)))]))

    ; sum-with-acc: list-of-Numbers -> Number -> Number
    (define (sum-with-acc li acc)
      (cond
        [(empty? li) acc]
        [(cons? li) (sum-with-acc (rest li) (+ (first li) acc))]))

Stepper.

Exercice:

    ; list-length: list-of-Any -> Number
    ; list-length-with-acc: list-of-Any -> Number -> Number

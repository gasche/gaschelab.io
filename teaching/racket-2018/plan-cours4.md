# Rappel sur le cours précédent

Programmes de calcul / programmes interactifs

# Chapitre 3: comment concevoir des programmes

<https://htdp.org/2018-01-06/Book/part_one.html#(part._ch~3ahtdp)>

Partir d'un problème et concevoir un programme pour le résoudre.

Spécification:
- Quelles sont les entrées du programme ?
- Quelles sont les sorties ?
- Quel est le lien entre les entrées et les sorties ?

Implémentation:
- comment structurer le programme en fonctions
- quelles fonctions existent déjà dans des bibliothèques externes ?
  (quel type de bibliothèques chercher ?)

Un bon programme décrit sa spécification en commentaires. Plus
lisible, plus facile à maintenir, permet le travail en équipe.

On va apprendre une "recette", ou "méthodologie" de conception des
programmes. Utile pour devenir programmeur, mais aussi tous les
métiers en interface avec la programmation, et les métiers où il faut
savoir décrire précisément un processus : ingéniérie, médecine,
journalisme...


# Section 3.1: concevoir une fonction

<https://htdp.org/2018-01-06/Book/part_one.html#(part._sec~3adesign-func)>

Notion de "domaine applicatif", ou juste "domaine" : la partie du
monde réel mis en jeu par le programme. (Exemple: base de donnée
universitaire, programme de navigation GPS, jeu vidéo...).

Comment en représenter les concepts clés ?

Différencier *informations* et *données*: par *données*, on entend des
informations mises en forme structurée, manipulable par nos
programmes. Problème d'affichage, de lecture, de
décomposition... (Reconnaissance vocale, etc.)

Utilisation de Racket: pas besoin de convertir informations<->données.

## Description de fonction

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Donner un ou plusieurs exemples
4. Code de la fonction

Exemples:

    ; We use numbers to represent euros and dollars
    ; euros->dollars : number -> number
    ; given 100, expect 115
    (define (euros->dollars x) (* x 1.15))
    
    ; move : pos vel -> pos
    ; (move p v) computes next position from p, at speed v
    ; given:
    ;   (make-pos 10 10) for p
    ;   (make-vel 5 3) for v
    ; expect: (make-pos 15 13)
    (define (move p v) ...)


# Chapitre 5. Structures

<https://htdp.org/2018-01-06/Book/part_one.html#(part._ch~3astructure)>

Rappel précédent:

- valeur absolue: taille de l'image
- signe : grandir ou rétrécir

Maintenant: comment structurer des données.

    ; size : the size of the object to draw
    ; decr? : is the object shrinking (#true) or expanding (#false)?
    (define-struct anim [size decr?])

On obtient automatiquement les fonctions suivantes

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

Exemple de la dernière fois:

    (require 2htdp/image)
    (require 2htdp/universe)
    
    (define (image n)
      (circle
       (if (>= n 0) n (- n))
       "solid" "red"))
    
    (define (next n)
      (if (>= n 0)
          (- n 1)
          (if (= n -100) 99 (- n 1))))
    
    (define start-state 100)
    (define (key-handler n evt)
      start-state)
    
    (big-bang start-state
        [to-draw image]
        [on-tick next]
        [on-key key-handler])

Nouvelle version

    (define-struct anim [size decr?])
    
    (define (image a)
      (circle (anim-size a) "solid" "red"))
    
    (define (incr a)
      (if (= size-limit (anim-size a))
          (make-anim size-limit #true)
          (make-anim (+ (anim-size a) 1) #false)))
    
    (define (decr a)
      (if (= 0 (anim-size a))
          (make-anim 0 #false)
          (make-anim (- (anim-size a) 1) #true)))
    
    (define (next a)
      (if (anim-decr? a) (decr a) (incr a)))  
    
    (define start-state (make-anim size-limit #true))
    (define (key-handler a evt)
      (if (anim-decr? a) (make-anim 100 #true) (make-anim 0 #false)))

Composition de structure : 

    (define-struct pos [x y])
    (define-struct vel [deltax deltay])
    
    (define-struct obj [image pos vel])
    
    (define (move p v)
      (make-pos
        (+ (pos-x p) (vel-x v))
        (+ (pos-y p) (vel-y y))))
    
    (define background (square 600 600 "solid" "white"))
    (define draw-obj obj
      (place-image
        (obj-image obj)
        (pos-x (obj-pos obj))
        (pos-y (obj-pos obj))
        background)



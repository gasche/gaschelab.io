# Sujet:

    (define-struct jeu-vidéo [nom prix])
    (define-struct sport [nom effectif prix-équipement])
    (define-struct musée [nom prix])
    
    ; une activité est soit:
    ; - un jeu vidéo
    ; - un sport collectif (effectif: le nombre de participants pour une partie)
    ; - une visite de musée
    ; - #false (une personne qui ne fait rien)
    
Pour chacune des fonctions ci-dessous, suivre la recette de conception
jusqu'au bout -- y compris une implémentation.

    ; nombre-de-participants: Activité -> Number
    ; taille-du-groupe: List-of-Activité -> Number
    ; prix-total: List-of-Activité -> Number

# Corrigé:

    (define-struct jeu-vidéo [nom prix])
    (define-struct sport [nom effectif prix-équipement])
    (define-struct musée [nom prix])
    
    ; une activité est soit:
    ; - un jeu vidéo
    ; - un sport collectif
    ; - une visite de musée
    ; - #false (une personne ne fait rien)
    
    ; nombre-de-participants: Activité -> Number
    ; taille-du-groupe: List-of-Activités -> Number
    ; prix-total: List-of-Activité -> Number
    
    (define zelda (make-jeu-vidéo "Zelda" 5))
    (define basket (make-sport "Basket" 5 20))
    (define orsay (make-musée "Musée d'Orsay" 15))
    
    ; nombre-de-participants: Activité -> Number
    ; le nombre de joueurs nécessaire pour chaque activité
    (check-expect (nombre-de-participants zelda) 1)
    (check-expect (nombre-de-participants basket) 5)
    (check-expect (nombre-de-participants orsay) 1)
    (check-expect (nombre-de-participants #false) 1)
    (define (nombre-de-participants acti)
      (cond
        [(jeu-vidéo? acti) 1]
        [(sport? acti) (sport-effectif acti)]
        [(musée? acti) 1]
        [(false? acti) 1]))
    
    ; taille-du-groupe: List-of-Activités -> Number
    ; le nombre de joueurs nécessaires pour pratiquer toutes ces activités en parallèle
    (check-expect (taille-du-groupe (cons zelda (cons basket '()))) 6)
    (define (taille-du-groupe actis)
      (cond
        [(empty? actis) 0]
        [(cons? actis) (+ (nombre-de-participants (first actis)) (taille-du-groupe (rest actis)))]))
    
    ; prix-activité: Activité -> Number
    ; prix de l'activité pour une personne
    (check-expect (prix-activité zelda) 5)
    (check-expect (prix-activité orsay) 15)
    (check-expect (prix-activité basket) 20)
    (check-expect (prix-activité #false) 0)
    (define (prix-activité acti)
      (cond
        [(jeu-vidéo? acti) (jeu-vidéo-prix acti)]
        [(sport? acti) (sport-prix-équipement acti)]
        [(musée? acti) (musée-prix acti)]
        [(false? acti) 0]))
    
    ; prix-total: List-of-Activités -> Number
    ; prix total de l'activité pour tous les joueurs
    (check-expect (prix-total (cons zelda (cons basket '()))) 105)
    (define (prix-total li)
      (cond
        [(empty? li) 0]
        [(cons? li) (+ (prix-total (rest li))
                       (* (prix-activité (first li)) (nombre-de-participants (first li))))]))

# Rappels

## Syntaxe

Expressions:

    (opération argument1 argument2 argument3)

Définition et utilisation de variable:

    (define nom-de-variable ...)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 nom-argument-3) ...)
    (define (square i) (* i i))

    (square 5)

## Opérations courantes

    (if truc machin chouette)

    (cond
      [truc bidule]
      [truc machine]
      [else chose])

nombres

    + - * /

images

    (require 2htdp/image)
    (rectangle 10 20 "solid" "blue")
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string->number number->string
    string=?

booléens

    and or not

prédicats de test

    image? number? string? boolean?

structures

    (define-struct anim [size decr?])

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

listes

    '() cons first rest empty? cons?

## Recette de conception

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

Exemple:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three
    ; possible states that a traffic light may assume

    ; the state of a traffic light is represented by TrafficLight
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

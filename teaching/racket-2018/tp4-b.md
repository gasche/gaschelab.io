# Rappels de syntaxe

Expressions:
  
    (opération argument1 argument2 ...)
  
Définition et utilisation de variable:
  
    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))  

    gregory

Définition et utilisation de fonction:
  
    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))
  
    (square 5)

## Rappels d'opérations courantes

nombres

```
+ - * /
```

images

```
(require 2htdp/image)
(circle 100 "solid" "red") square star
above beside
image-width image-height
```

chaînes de caractères (strings)

```
string-length string-append string-ith
string->number number->string
string=?
```

booléens

```
and or not
```

prédicats de test

```
image? number? string? boolean?
```

# Sujet

Écrire les fonctions dont les descriptions suivent:

```
; wider : Image Image -> String
; (wider img1 img2) returns "wider" if the first image is wider than the second, "thinner" otherwise
; (wider img1 img2) renvoie "wider" si la première image est plus large que la seconde, et "thinner" sinon
; given (rectangle 10 20 "solid" "blue") and (rectangle 20 30 "solid" "red"), expect "thinner"
; given (rectangle 20 10 "solid" "blue") and (rectangle 15 10 "solid" "red"), expect "wider"

; same-first-letter: String String -> Boolean
; (same-first-letter? s1 s2) returns #true if s1 and s2 have the same first letter, #false otherwise
; (same-first-letter? s1 s2) renvoie #true si s1 et s2 ont la même première lettre, et #false sinon
; given "foo" and "bar", expect #false
; given "foo" and "fruit", expect #true

; sum-halves : Number -> Number
; (sum-halves n) returns the sum of halves of numbers from 0 to n
;                (the half of n is n/2)
; (sum-halves n) renvoie la somme des moitiés des nombres de 0 à n
;                (la moitié de n est n/2)
; given 0, expect 0
; given 4, expect 5
```


# Corrigé

```racket
(require 2htdp/image)

(define (wider i1 i2)
  (if (>= (image-width i1) (image-width i2))
    "wider"
    "thinner"))

; tests, facultatif
(define (rect w h) (rectangle w h "solid" "blue"))
(check-expect (wider (rect 20 10) (rect 10 20)) "wider")
(check-expect (wider (rect 5 10) (rect 10 20)) "thinner")


(define (same-first-letter s1 s2)
  (string=? (string-ith s1 0) (string-ith s2 0)))

(check-expect (same-first-letter "fool" "fac") #true)
(check-expect (same-first-letter "université" "fac") #false)


(define (half n) (/ n 2))
(define (sum-halves n)
  (if (= n 0)
    0
    (+ (half n) (sum-halves (- n 1)))))

(check-expect (sum-halves 0) 0)
(check-expect (sum-halves 3) 3)
```

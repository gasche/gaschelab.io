# Sujet

## Exercice 1

Implémenter la fonction suivante:

    ; different-first-and-second-letter? : String -> Bool
    ; are the first and second letter of the input different?
    (check-expect (different-first-and-second-letter? "yacine") #true)
    (check-expect (different-first-and-second-letter? "lloris") #false)

### Corrigé

    ; different-first-and-second-letter? : String -> Bool
    ; are the first and second letter of the input different?
    (check-expect (different-first-and-second-letter? "yacine") #true)
    (check-expect (different-first-and-second-letter? "lloris") #false)

    (define (different-first-and-second-letter? str)
      (and (>= (string-length str) 2)
           (not (string=? (string-ith str 0) (string-ith str 1)))))

## Exercice 2

On définit deux structures de formes géométriques avec des couleurs.

    ; hauteur: Number
    ; largeur: Number
    ; couleur: String
    (define-struct forme-rectangle [hauteur largeur couleur])

    ; rayon: Number
    ; couleur: String
    (define-struct forme-cercle [rayon couleur])

On appelle `Forme` un valeur qui est soit une `forme-rectangle` soit
une `forme-cercle`:

    ; A Forme is one of the following:
    ; - a forme-rectangle
    ; - a forme-cercle

Vous devez implémenter les deux fonctions suivantes:

    ; nouvelle-couleur: Forme String -> Forme
    ; takes a Forme and a color, and returns a modified Forme of that color
    (check-expect
      (nouvelle-couleur (make-forme-rectangle 10 20 "blue") "red")
      (make-forme-rectangle 10 20 "red"))
    (check-expect
      (nouvelle-couleur (make-forme-cercle 10 "blue") "green")
      (make-forme-cercle 10 "green"))

et:

    (require 2htdp/image)
    ; dessine-forme: Forme -> Image
    ; converts a Forme into an image
    (check-expect
      (dessine-forme (make-forme-rectangle 10 20 "blue"))
      (rectangle 10 20 "solid" "blue"))
    (check-expect
      (dessine-forme (make-forme-cercle 10 "blue"))
      (circle 10 "solid" "blue"))

### Corrigé

    ; hauteur: Number
    ; largeur: Number
    ; couleur: String
    (define-struct forme-rectangle [hauteur largeur couleur])

    ; rayon: Number
    ; couleur: String
    (define-struct forme-cercle [rayon couleur])

    ; A Forme is one of the following:
    ; - a forme-rectangle
    ; - a forme-cercle

    ; nouvelle-couleur: Forme String -> Forme
    ; takes a Forme and a color, and returns a modified Forme of that color
    (check-expect
      (nouvelle-couleur (make-forme-rectangle 10 20 "blue") "red")
      (make-forme-rectangle 10 20 "red"))
    (check-expect
      (nouvelle-couleur (make-forme-cercle 10 "blue") "green")
      (make-forme-cercle 10 "green"))

    (define (nouvelle-couleur forme coul)
      (cond
        [(forme-rectangle? forme)
         (make-forme-rectangle
          (forme-rectangle-hauteur forme)
          (forme-rectangle-largeur forme)
          coul)]
        [(forme-cercle? forme)
         (make-forme-cercle
          (forme-cercle-rayon forme)
          coul)]))

    (require 2htdp/image)
    ; dessine-forme: Forme -> Image
    ; converts a Forme into an image
    (check-expect
      (dessine-forme (make-forme-rectangle 10 20 "blue"))
      (rectangle 10 20 "solid" "blue"))
    (check-expect
      (dessine-forme (make-forme-cercle 10 "blue"))
      (circle 10 "solid" "blue"))

    (define (dessine-forme f)
      (cond
        [(forme-rectangle? f)
         (rectangle
          (forme-rectangle-hauteur f)
          (forme-rectangle-largeur f)
          "solid"
          (forme-rectangle-couleur f))]
        [(forme-cercle? f)
         (circle
          (forme-cercle-rayon f)
          "solid"
          (forme-cercle-couleur f)
          )]))


## Exercice 3

    ; has-strictly-negative?: List Number -> Bool
    ; has-strictly-positive? checks whether one number in the list
    ; is strictly negative (< n 0)
    (check-expect (has-strictly-negative? (cons 1 '())) #false)
    (check-expect (has-strictly-negative? (cons 2 (cons -1 '()))) #true)

### Corrigé

    ; has-strictly-negative?: List Number -> Bool
    ; has-strictly-positive? checks whether one number in the list
    ; is strictly negative (< n 0)
    (check-expect (has-strictly-negative? (cons 1 '())) #false)
    (check-expect (has-strictly-negative? (cons 2 (cons -1 '()))) #true)

    (define (has-strictly-negative? list)
      (cond
        [(empty? list) #false]
        [(cons? list)
         (or
          (< (first list) 0)
          (has-strictly-negative? (rest list)))]))

## Exercice bonus

Suivre la recette de conception pour implémenter une fonction
`premiere-lettre` qui renvoie la première lettre d'un mot.

    ; le mot sera représenté par String
    ; premiere-lettre: String -> 1String
    ; quand on lui donne une chaîne de caractère, on récupère la première lettre
    (check-expect (premiere-lettre "bibliothèque") "b")
    (define (premiere-lettre mot) (string-ith mot 0))

# Rappels

## Syntaxe

Expressions:

    (opération argument1 argument2 ...)

Définition et utilisation de variable:

    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))

    gregory

Définition et utilisation de fonction:

    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))

    (square 5)

## Opérations courantes

    (if truc machin chouette)
    (cond
      [truc bidule]
      [truc machine]
      [else chose])

nombres

    + - * /

images

    (require 2htdp/image)
    (rectangle 10 20 "solid" "blue")
    (circle 100 "solid" "red") square star
    above beside
    image-width image-height

chaînes de caractères (strings)

    string-length string-append string-ith
    string->number number->string
    string=?

booléens

    and or not

prédicats de test

    image? number? string? boolean?

structures

    (define-struct anim [size decr?])

    (make-anim s dir)
    (anim-size anim)
    (anim-decr? anim)
    (anim? x)

listes

    '() cons first rest empty? cons?

## Recette de conception

1. Réfléchir au choix de données, l'expliquer si nécessaire.
2. Donner la signature.
3. Expliquer ce que fait la fonction.
4. Donner un ou plusieurs exemples
5. Code de la fonction

Exemple:

    ; A TrafficLight is one of the following Strings:
    ; – "red"
    ; – "green"
    ; – "yellow"
    ; interpretation the three strings represent the three
    ; possible states that a traffic light may assume

    ; the state of a traffic light is represented by TrafficLight
    ; TrafficLight -> TrafficLight
    ; yields the next state given current state s
    (check-expect (traffic-light-next "red") "green")
    (define (traffic-light-next s)
      (cond
        [(string=? "red" s) "green"]
        [(string=? "green" s) "yellow"]
        [(string=? "yellow" s) "red"]))

# Rappels de syntaxe

Expressions:
  
    (opération argument1 argument2 ...)
  
Définition et utilisation de variable:
  
    (define nom-de-variable ?)
    (define gregory (string-append "bon" " " "jour"))  

    gregory

Définition et utilisation de fonction:
  
    (define (nom-de-fonction nom-argument1 nom-argument2 ...) ?)
    (define (square i) (* i i))
  
    (square 5)

## Rappels d'opérations courantes

nombres

```
+ - * /
```

images

```
(require 2htdp/image)
(circle 100 "solid" "red") square star
above beside
image-width image-height
```

chaînes de caractères (strings)

```
string-length string-append string-ith
string->number number->string
string=?
```

booléens

```
and or not
```

prédicats de test

```
image? number? string? boolean?
```

# Sujet

Écrire les fonctions dont les descriptions suivent:

```
; longer: String String -> String
; (longer s1 s2) returns "first" if the string s1 is longer than s2, and "second" otherwise
; (longer s1 s2) renvoie "first" si la chaîne s1 est plus longue que s2, et "second" sinon
; given:
;   "fac" for s1
;   "université" for s2
; expect: "second"

; square? : Image -> Bool
; (square? img) returns #true if the image img is square (as high as wide)
; (square? img) renvoie #true si l'image est carrée (aussi large que haute)
; given (rectangle 10 20 "solid" "red"), expect #false
; given (square 10 "solid" "blue"), expect #true

; sum-cube : Number -> Number
; (sum-cube n) returns the sum of cubes of numbers from 0 to n
;              (the cube of k is the number k*k*k)
; (sum-cube n) renvoie la somme des cubes des nombres de 0 à n
;              (le cube de k est le nombre k*k*k)
; given 0, expect 0
; given 3, expect 36
```


# Corrigé

```racket
(define (longer s1 s2)
  (if (>= (string-length s1) (string-length s2))
     "first"
     "second"))

; tests, facultatif
(check-expect (longer "fac" "université") "second")
(check-expect (longer "bagne" "fac") "first")


(require 2htdp/image)
(define (square? img)
  (= (image-width img) (image-height img)))

(define (rect w h) (rectangle w h "solid" "blue"))
(check-expect (square? (rect 10 10)) #true)
(check-expect (square? (rect 20 10)) #false)


(define (cube n) (* n n n))
(define (sum-cube n)
  (if (= n 0)
    0
    (+ (cube n) (sum-cube (- n 1)))))

(check-expect (sum-cube 0) 0)
(check-expect (sum-cube 3) 36)
```

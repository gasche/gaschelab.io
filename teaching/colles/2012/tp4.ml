(** Question 1 *)
let add (r, i) (r', i') = (r +. r', i +. i');;
let mult (r, i) (r', i') = (r *. r' -. i *. i', r *. i' +. i' *. r);;
let norme2 (r, i) = r *. r +. i *. i;;

(** Question 2 *)
let limite_iterations = 50
let limite_norme2 = 4.

let mandelbrot const =
  let rec loop n comp =
    if norme2 comp > limite_norme2 || n = limite_iterations then (n, comp)
    else loop (n + 1) (add const (mult comp comp)) in
  loop 0 (0., 0.);;

(** Question 3 *)
let normalise n nsize = float_of_int n /. float_of_int nsize;;
let point (xsize, ysize) (x, y) =
  (2. *. normalise x xsize -. 1.5,
   2. *. normalise y ysize -. 1.)

(** Question 4 *)
let mandelbrot_texte xsize ysize =
    for y = 0 to ysize-1 do
      for x = 0 to xsize - 1 do
        let (n, res) =  mandelbrot (point (xsize, ysize) (x, y)) in
        print_char (if norme2 res > limite_norme2 then `-` else `X`);
      done;
      print_newline ()
  done
;;

(** Question 5*)
#open "graphics";;

let caml_couleur (r, g, b) =
  let norme x = int_of_float (x *. 255.9) in
  rgb (norme r) (norme g) (norme b)
;;

(** Question 6 *)
let couleur (n, _) =
  let k = normalise n limite_iterations in
  caml_couleur (k, k, k)
;;

(** Question 7 *)
let mandelbrot_graphique couleur xsize ysize =
  open_graph (printf__sprintf " %dx%d" xsize ysize);
  for x = 0 to xsize - 1 do
    for y = 0 to ysize - 1 do
      set_color (couleur (mandelbrot (point (xsize, ysize) (x, y))));
      plot x y;
    done
  done;
  let _ = read_key () in
  close_graph ()
;;

(** Question 8 *)
let couleur2 (n, (r, i)) =
  if n < limite_iterations then caml_couleur (0.,0.,0.)
  else
    let r2, i2, n2 = r *. r, i *. i, norme2 (r, i) in
    if n2 < 1E-6 then caml_couleur (0.,0.,0.)
    else
      let normalize x = x /. norme2 (r, i) in
      caml_couleur (normalize r2, 0., normalize i2)
;;

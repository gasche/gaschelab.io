(** Gabriel Scherer, colles d'informatique MPSI1-MPSI4
    
    Corrigé du TP2
    
    Faites ce que vous voulez du code.
*)


(** Question 1 *)

(* version naïve : complexité exponentielle (faire un dessin) *)
let rec fib = function
| 0 | 1 -> 1
| n -> fib (n - 1) + fib (n - 2);;

(* idée : on stocke le coupe (u_k, u_{k+1}), qui suffit pour calculer
u_{k+2}, et ainsi de proche en proche tous les termes jusqu'à u_n en
un temps linéaire *)
let fib n =
  let rec fib' (a, b) = function
  | 0 -> a
  | n -> fib' (b, a+b) (n-1) in
  fib' (1, 1) n;;

(* Il existe des versions plus efficaces, en log(N) par exemple;
   on a vu au premier TP une version en temps quasi-constant, sur les flottants *)


(** Question 2 *)

let rec pow x = function
| 0 -> 1
| n -> pow (x * x) (n / 2) * (if n mod 2 = 0 then 1 else x);;


(** Question 3 *)

type solution = Aucune_racine | Racine_simple of float | Racine_double of float * float;;

let solve (a, b, c) =
  let delta = b *. b -. 4. *. a *. c in
  let racine signe = (-.b +. signe *. sqrt delta) /. (2. *. a) in
  if delta < 0. then Aucune_racine
  else if delta = 0. then Racine_simple (racine 0.)
  else Racine_double (racine 1., racine (-1.));;


(** Question 4 *)

(* La fonction donnée (est_ce_moi) comporte trois erreurs :
   - erreur sans importance : mon_tel est de type 'string', au lieu de 'int'
   - erreur peu importante : le cas Inconnu n'est pas géré
   - erreur grave : le code est subtilement faux, il renvoie toujours 'true'.
     En effet, le motif (Mail mon_mail) ne teste *pas* l'égalité du mail donné
     avec la variable 'mon_mail' : il déclare une nouvelle variable mon_mail
     correspondant à la valeur donnée en paramètre, en écrasant la variable
     précédente. En particulier, il réussit toujours (quand on donne un mail).
*)


(** Question 5 *)
type robot = Stop | Move of (float * float) * robot;;

let rec arrivee position = function
| Stop -> position
| Move (nouvelle_position, suite) -> arrivee nouvelle_position suite;;

(** Question 6 *)
let rec taille = function
| Stop -> 0
| Move (_, suite) -> 1 + taille suite;;

(** Question 7 *)
let rec last elem = function
| [] -> elem
| t :: q -> last t q;;

let rec length = function
| [] -> 0
| _ :: q -> 1 + length q;;

(** Question 8 *)

(* idée : on utilise la fonction (div d n) :
   d parcours l'ensemble des diviseurs de n en partant de 2
   si ce n'est pas un diviseur, on essaie avec (d+1)
   sinon, on sait que c'est le plus petit diviseur rencontré (donc il est premier) :
     on l'ajoute à la liste, et on calcule la liste des diviseurs de (n/d) *)
let diviseurs n =
  let rec div d n =
    if n = 1 then []
    else if n mod d > 0 then div (d+1) n
    else d :: div d (n / d) in
  div 2 n;;

(** Question 9 *)
let rec map f = function
| [] -> []
| t :: q -> let a = f t in a :: map f q;;

(** Question 10 *)
let rec rev_append a b = match a with
| [] -> b
| t :: q -> rev_append q (t :: b);;

let rev li = rev_append li [];;

(** Question 11 *)
let rec insert e = function
| [] -> [e]
| t :: q ->
    if e <= t then e :: t :: q
    else t :: insert e q;;

let rec sort = function
| [] -> []
| t :: q -> insert t (sort q);;


(** Question 12 *)

(* On représente notre file comme une liste de couples :
   - quand l'utilisateur veut ajouter un élément, on le met dans la liste de gauche au début
   - quand il veut retirer un élément, on prend le premier de la liste de droite
   
   Cela veut dire que les éléments de la liste de gauche sont dans
   l'ordre dans lequel ils ont été insérés, et ceux de la liste de
   droite sont dans l'ordre inverse (pour pouvoir récupérer le premier
   inséré rapidement, sans devoir parcourir toute la liste).

   Au cours de l'utilisation de la file, la liste de gauche se remplit
   et celle de droite se vide.  Quand elle devient complètement vide,
   il faut transférer ceux de la liste gauche pour les mettre dans la
   liste droite (à l'envers).
*)

let enqueue elem (debut, fin) = (elem :: debut, fin);;

let rec dequeue = function
| ([], []) -> failwith "file vide"
| (debut, elem :: fin) -> elem, (debut, fin) (* on renvoie un couple : élément retiré et nouvelle file *)
| (debut, []) -> dequeue ([], rev debut);; (* on fait passer les éléments de la gauche vers la droite *)

(* La plupart de ces opérations sont en temps constant (ajout/retrait
   d'un élément en tête de liste) sauf de temps en temps quand il faut
   transférer les éléments.

   En fait, on peut considérer que les appels sont constants "en
   moyenne" : le transfert est en O(N) où N est le nombre d'éléments
   dans le début de la file, mais ce nombre est borné par le nombre
   d'insertions faites par l'utilisateur. Si l'on compte un peu plus
   de temps pour chaque insertion (mais un temps constant), on peut
   considérer que les suppressions se font en temps constant "amorti"
   (il existe une version en temps constant non amorti de cet
   algorithme; elle est (beaucoup) plus compliquée et moins jolie).
*)


(** Question 13 *)
(* Je vous invite à regarder le code de ces fonctions dans les sources de caml light *)

(** Question 14 *)
(* Je ne traiterai pas ce point (de détail en prépa) dans le
   corrigé. Envoyez-moi un mail si vous avez des questions à ce sujet. *)

(** Question 15 *)
(* Idée : définir deux types différents avec le même nom, et mélanger leurs valeurs *)
type truc = Truc;;
let truc = Truc;;
type truc = Autre_truc;;
let autre_truc = Autre_truc;;

truc <> autre_truc;; (* boum ! *)

(** Question 16 *)
(* à lire dans un éditeur avec une police correcte, sinon ça ne marche pas.
   http://chris.com/ascii/art/html/animals_js.html

        _    _
       ( \__//) 
       .'     )
    __/b d  .  )
   (_Y_`,     .)
    `--'-,-'  )
         (.  )
         (   )
        (   )
       ( . )         .---.
      (    )        (     )
      (   . )      (  .    )
      (      )    (      .  ),
      ( .     `"'`  .       `)\
       (      .              .)\
       ((  .      .   (   .   )\\
       ((       .    (        ) \\
        ((     )     _( .   . )  \\
        ( ( .   )"'"`(.(     )   ( ;
        ( (    )      ( ( . )     \'
         |~(  )        |~(  )
         | ||~|        | ||~|
    jgs  | || |        | || |    
        _| || |       _| || |
       /___(| |      /___(| |
          /___(         /___(
*)

\documentclass[10pt]{tpcaml}

% TP caml � LLG, D�but F�vrier 2010
% par Gabriel Scherer
% Version modifi�e des colles de Victor Nicollet

\tp{3}{Programmation imp�rative, biblioth�que, tris\\ }

\begin{document}

  \maketitle

\section{Programmation imp�rative}

Nous allons maintenant r�capituler rapidement les aspects
``imp�ratifs'' de \camli : boucles (\code{for}, \code{while}) et
champs modifiables (r�f�rences, tableaux...). J'ai choisi de vous
pr�senter tout �a seulement apr�s la programmation r�cursive parce que
c'est quelque chose que vous utilisez trop souvent\footnote{Malheureux
  h�ritage de la programmation Maple, peut-�tre ?}, alors que les
solutions sans boucles et sans r�f�rences sont parfois plus simples et
plus lisibles.

Je vous en parle maintenant parce qu'il arrive que ce soit la bonne
solution, mais attention, �a ne veut pas dire qu'� partir de
maintenant, vous devez vous en servir tout le temps !

\subsection{Champs modifiables}

En \caml, les variables ne sont pas \emph{modifiables} : une fois
qu'une variable a �t� d�clar�e par un \code{let}, sa valeur ne change
pas (jusqu'� la d�claration suivante). Comme en math�matiques : vous
n'�crivez jamais dans une copie ``et maintenant n vaut n+1'' !

Les informaticiens, contrairement aux math�maticiens, ont parfois
besoin de \emph{modifier} des valeurs (mais on peut s'en
passer). Il existe donc plusieurs objets du langage \caml permettant
de faire cela.

R�f�rences : si \code{'a} est un type \caml, \code{'a ref}
d�signe le type des \emph{r�f�rences} contenant une valeur de type
\code{'a}. On peut voir une r�f�rence \code{x} comme un tiroir : on
peut l'ouvrir pour regarder � l'int�rieur (c'est \code{!x}), on voit
un objet de type \code{'a}, ou bien changer son contenu pour y mettre
la valeur \code{v} (\code{x := v}).

\begin{codesection}
let test =\\
\ \ let a, b = ref 1, ref 2 in\\
\ \ let c, d = a, ref a in\\
\ \ a := !a + !b; d := c; !(!d) + !a;;
\end{codesection}

Pour cr�er une nouvelle r�f�rence, on utilise la fonction \code{ref}
en lui donnant une valeur de d�part (la valeur que contiendra le
tiroir avant que son contenu ne soit modifi�). Attention � la
subtilit� : quand on utilise l'op�rateur \code{:=}, on change le
\emph{contenu} de la r�f�rence, pas la r�f�rence elle-m�me (qui
d�signe toujours le m�me tiroir) !

\begin{codesection}
  let a, b = ref 1, ref 1;; let c = a;;\\
  let test1 = (a = b, a == b);;\\
  b := 2;;\\
  let test2 = (a = b, a == c);;
\end{codesection}

\begin{question}
  Quelle est la valeur de la variable \code{test} ?
\end{question}

\vspace{12pt}

\begin{question}
  Que valent les variables \code{test1} et \code{test2} ? Que fait l'op�rateur \code{==} ?
\end{question}

\vspace{12pt} 

Les cases des tableaux et des cha�nes de caract�res sont aussi des
champs modifiables : \code{tab.(i)} et \code{str.[i]} permettent
d'obtenir la valeur en i-�me position (en partant de 0) du tableau
\code{tab} et de la cha�ne \code{str}. Pour les modifier on n'utilise
pas ``\code{:=}'' mais ``\code{<-}'' : \code{tab.(i) <- v}.

Les enregistrements (types produits utilisant la syntaxe
\code{\{ ... \}}) peuvent avoir certains champs modifiables, si c'est
pr�cis� � la d�claration par le mot-cl� \code{mutable} :\\
\code{type 'a reference = \{mutable contents : 'a\};;}

On peut alors modifier le champ avec la syntaxe \code{x.champ <- v}.

\vspace{12pt}

\begin{question}
  On peut red�finir les r�f�rences en partant des champs
  modifiables. D�finir \code{get} (pour \code{!}), \code{set} (pour
  \code{:=}) et la fonction \code{new\_ref} (pour \code{ref}) en
  utilisant le type \code{reference} d�fini au dessus.
\end{question}

%r�f�rences, tableaux, enregistrements
\subsection{Boucles}

Sans surprise :\\
\code{for <nom> = <expr1> to <expr2> do <expr3> done} : faire
  parcourir les entiers de \code{expr1} � \code{expr3} � la variable
  \code{nom} en �valuant \code{expr3} pour chaque valeur\\
\code{while <expr1> do <expr2> done} : �valuer \code{expr2} tant que le bool�en \code{expr1} vaut \code{true}

\vspace{12pt}

\begin{question}
  Utiliser le crible d'eratosth�ne pour calculer la somme des nombres
  premiers entre $0$ et $10^6$.
\end{question}

\subsection{Exceptions}

Les exceptions sont une mani�re d'interrompre une partie d'un
programme en cas d'erreur. Par exemple, si vous avez une grosse
formule math�matique � calculer, et qu'en plein calcul vous vous
apercevez que vous devez diviser 0, vous allez vous arr�ter et vous
plaindre que la formule n'est pas bien d�finie. \camli sait faire
pareil.

\begin{codesection}
  \#exception Erreur of string;;\\
  let boum () = raise (Erreur "boum");;\\
  try (boum (); "message") with\\
  | Exit -> "sortie"\\
  | Erreur message -> "erreur : " \^ message\\
  | \_ -> "exception inconnue"
\end{codesection}

Les expressions sont des objets de type \texttt{exn} qui ressemblent
beaucoup aux types sommes d�finis dans le pr�c�dent TP : ce sont des
constructeurs, qui peuvent comporter des arguments, et sont d�clar�s
par le mot-cl� \texttt{exception}.

Quand on a trouv� une erreur, on peut \emph{lancer} une expression
avec la fonction \texttt{raise} : elle prend une expression en
param�tre, et interromp le calcul (en particulier, tout ce qui devait
se passer ensuite dans le programme n'est pas ex�cut�).

Cela permet de faire des erreurs qui stoppent compl�tement le
calcul. Parfois, on voudrait plut�t d�tecter l'erreur et utiliser une
solution adapt�e pour continuer le programme (par exemple si l'erreur
est ``plus de papier dans l'imprimante'', il suffit de demander
� l'utilisateur de rajouter du papier avant de continuer, au lieu
d'annuler compl�tement l'impression en cours). On peut
\emph{rattrapper} une exception avec la construction \texttt{try
  <expr> with <filtrage>}. Cela se pr�sente un peu comme un
\texttt{match .. with}, mais le comportement est diff�rent :
\begin{itemize}
\item si l'�valuation de \texttt{<expr>} ne provoque aucune exception, on renvoie sa valeur
\item sinon, on effectue le filtrage sur la valeur de l'exception envoy�e
\end{itemize}

\begin{codesection}
  let existe element tableau =\\
\ \    let i, trouve = ref 0, ref false in\\
\ \    while not !trouve \&\& !i < vect\_length tableau do\\
\ \ \ \      trouve := tableau.(!i) = element;\\
\ \ \ \      incr i\\
\ \    done;\\
\ \    !trouve\\
\end{codesection}

Plusieurs exceptions sont pr�d�finies en \camli (vous les trouverez
dans le manuel, module \texttt{exc}). Vous rencontrerez (et/ou
utiliserez) sans doute entre autres \texttt{Exit}, qui permet
simplement de sortir d'un calcul, et \texttt{Failure of string} qui
permet d'indiquer la cause de l'erreur. On peut aussi utiliser la
fonction \texttt{failwith} qui lance \texttt{Failure}, et est d�finie
ainsi :
\code{let failwith str = raise (Failure str)}

\vspace{12pt}

\begin{question}
  R��crire la fonction \texttt{existe} pour utiliser une boucle
  \texttt{for}, sans parcourir plus d'�l�ments du tableau.
\end{question}

\vspace{12pt}

\begin{question}
  Quel est le type de \texttt{raise} ? Pourquoi ?
\end{question}


\subsection{Dangers}
\begin{codesection}
let nouvelle\_matrice n p x =\\
\ \ make\_vect n (make\_vect p x);;\\
let mat = nouvelle\_matrice 3 3 0;;\\
 mat.(0).(1) <- 2; mat;;
\end{codesection}

On veut cr�er un tableau � deux dimensions, sans utiliser\footnote{�a forge le caract�re} la fonction d�j� toute faite \code{make\_matrix}.

\vspace{12pt} 

\begin{question}
  Quel est le probl�me avec \code{mat} ?\\
  Coder (correctement) \code{nouvelle\_matrice} avec une boucle.\\
  Coder \code{nouvelle\_matrice} avec \code{init\_vect}.\\
  Coder \code{nouveau\_pave} qui cr�e un tableau en trois dimensions.
\end{question}

\section{Boite � outils}

Vous avez globalement vu l'ensemble des concepts \camli dont vous
aurez besoin. Voici maintenant quelques \emph{biblioth�ques}, des
ensembles de fonctions d�j� existantes (vous en avez d�j� rencontr�es
plusieurs) qu'il est parfois utile de pouvoir utiliser sans les
recoder.

Pour s'informer, la meilleure solution est d'aller consulter la section du manuel :
http://caml.inria.fr/pub/docs/manual-caml-light/

Dans cette partie, vous aurez besoin de consulter la documentation. Si
vous n'avez pas d'acc�s internet, c'est le moment de se plaindre.

Les fonctions de ces biblioth�ques sont organis�s dans des
\emph{modules} th�matiques. Par exemple le module \code{list} contient
toutes les fonctions pour manipuler des listes.

On distingue deux types de modules :
\begin{itemize}
\item les modules ``core'' : les fonctions des modules du core sont accessibles directement par leur nom, par exemple \code{map} dans le module \code{list}
\item les modules ``standard'' : les fonctions des modules standards
  doivent �tre pr�fix�es par le nom du module : pour appeler la
  fonction \code{init} du module \code{random}, on utilise
  \code{random\_\_init}. Notez le \emph{double} underscore.
\end{itemize}

\begin{codesection}
  random\_\_init ();;\\
  \#open ``random'';;\\
  init ();;
\end{codesection}
Il est possible de se passer du nom du module en pr�fixe en utilisant
une directive \#open. Dans la suite du code il suffit alors de mettre
le nom des fonctions du module, comme pour les modules cores.


\subsection{stack, queue}

\texttt{stack} contient des fonctions pour manipuler des \emph{piles}
(premier entr� dernier sorti), et \texttt{queue} des \emph{files}
(premier entr� premier sorti). Ces structures de donn�es se manipulent
de fa�on imp�rative.

\begin{question}
  �crire des fonctions de conversion de pile en liste (et
  r�ciproquement) et de file en liste (idem).
\end{question}

\subsection{set, map}

\texttt{set} et \texttt{map} sont des structures de donn�es plus
sophistiqu�es, qui permettent de stocker des �l�ments et d'y acc�der
rapidement (en un temps logarithmique). \texttt{set} repr�sente un
ensemble (on peut mettre des �l�ments et se demander des �l�ments sont
dedans) et \texttt{map} une table d'association : comme un tableau, mais
au lieu d'indexer par un entier on peut indexer par n'importe quel
type dont on sait comparer les �l�ments.

Pour faire des tables d'association, on peut aussi utiliser le module
de tables de hachage \texttt{hashtbl}. Il est plus adapt� aux
contextes imp�ratifs, et \texttt{map} aux contextes
fonctionnels/r�cursifs (sans modification de valeurs).

\vspace{12pt}

\begin{question}
  �crire une fonction qui ajoute les entiers de 0 � 10000 dans une
  liste, puis teste pour chaque entier de 0 � 10000 s'il appartient
  � la liste.

  Faire de m�me avec un \texttt{set}, et comparer les temps
  d'ex�cution.
\end{question}


\vspace{12pt}
\begin{question}
  La complexit� exponentielle de la version na�ve de fibonnaci vient
  du fait que la fonction est rappell�e un grand nombre de fois avec
  les m�mes arguments. On peut l'optimiser na�vement de la mani�re
  suivante : on cr�e une fonction fibonacci qui se ``souvient'' des
  r�ponses qu'elle a d�j� donn� : si on lui demande la valeur en un
  nombre pour la premi�re fois, elle calcule le r�sultat et le stocke
  dans un ``cache'' (une \texttt{map}), et ensuite elle redonne
  directement le r�sultat calcul�. Impl�menter une telle fonction
  fibonnaci optimis�e.
  
  Implementer une fonction \texttt{memoize : ('a -> 'b) -> ('a -> 'b)}
  qui est capable de ``mettre en cache'' n'importe quelle fonction (on
  utilisera pour cr�er la \texttt{map} la fonction de comparaison
  g�n�rique \texttt{compare}).
\end{question}

\subsection{random}


\begin{question}
  �crire une fonction qui permute al�atoirement tous les �l�ments d'un
  tableau.
\end{question}

\subsection{printf}

\begin{codesection}
\#open "printf";;\\
printf "\%d$\backslash$n" 5;;\\
printf "J'ai \%d \%ss !$\backslash$n" 5 "pomme";;
\end{codesection}

Printf est un module d'affichage polyvalent. Il est aussi pratique que
sa documentation est ind�chiffrable.

\vspace{12pt}

\begin{question}
  Aller lire, et essayer de comprendre une partie de, la documentation
  du module \texttt{printf}.  
  Que fait la fonction \texttt{sprintf} ?
\end{question}

\section{Questions difficiles}

\begin{question}
  �crire une fonction \texttt{finie} qui renvoie vrai si et seulement
  si la liste pass�e en argument est de longueur finie (et qui termine
  dans tous les cas, bien s�r).
\end{question}

\vspace{20pt}

On essaiera dans cette partie de d�finir des fonctions r�cursives sans
utiliser le mot-cl� \texttt{rec}. C'est inutile en \caml, mais cela
repose sur des id�es int�ressantes, et ce n'est pas facile.

\subsection{D�-r�cursifier}

On travaillera sur l'embl�matique fonction \texttt{fac} :
\code{let rec fac n = if n = 0 then 1 else n * fac (n - 1)}

On d�finit la fonction \texttt{fix} suivante :
\code{let rec fix f = fun x -> f (fix f) x}

\vspace{12pt}

\begin{question}
  �crire une fonction non r�cursive \texttt{fac\_derec} telle qu'on puisse d�finir \texttt{fac} ainsi :
  \code{let fac = fix fac\_derec;;}
\end{question}

\vspace{12pt}

\texttt{fac\_derec} repr�sente une version ``d�r�cursifi�e'' (sans
appel r�cursif) de \texttt{fac}. On peut syst�matiquement
d�r�cursifier les fonctions r�cursive, et retrouver le comportement
initial en utilisant \texttt{fix}.

On a ici s�par� deux choses : le corps de la fonction (qui effectue le
vrai calcul) et l'application de la r�cursivit� (qui est rel�gu�e
� \texttt{fix}, qui est elle-m�me d�finie en utilisant
\texttt{rec}). On cherche maintenant � �crire d'autres versions de
\texttt{fix}, qui font la m�me chose sans utiliser \texttt{rec}.

\subsection{R�cursion par effet de bord}

\begin{question}
  �crire une version de \texttt{fix} utilisant des r�f�rences.

  Voici un squelette de code � compl�ter :\\
  \code{\ \ \ \ let fix f = let res = ref (fun \_ -> hd []) in ... ; !res}
\end{question}

\subsection{R�cursion par type r�cursif}

\vspace{12pt}

\begin{question}
  On d�finit les fonctions suivantes :\\
  \code{\ \ \ \ let auto x = x x}\\
  \code{\ \ \ \ let fix f = auto (fun x -> f (auto x))}

  Montrer en utilisant un papier et un stylo, et en faisant les
  r�ductions � la main, que \code{fix f = f (fix f)}.

  Pourquoi �a ne marche pas en \camli ?
\end{question}

\vspace{12pt}

\begin{question}
  L'astuce est d'utiliser un \emph{type r�cursif}.
  
  Utiliser le type suivant pour d�finir une version ``qui marche'' de \texttt{auto} :\\
  \code{\ \ \ \ type 'b auto = Auto of ('b auto -> 'b)}

  Pourquoi a-t-on choisi cette d�finition du type \texttt{'b auto} ?
\end{question}

\vspace{12pt}

\begin{question}
  D�finir correctement (en utilisant \texttt{auto}) le \texttt{fix} de la question 16. Tester avec \texttt{fac}.\\
  Utiliser plut�t \code{let fix f = auto (fun x -> f (fun y -> auto x y))}.
\end{question}

\end{document}

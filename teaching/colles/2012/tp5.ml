(** Question 1 *)
let mouvements = [2,1; 1,2; -1,2; -2,1; -2,-1; -1,-2; 1,-2; 2,-1];;

(** Question 2 *)
let deplace (x, y) (dx, dy) = (x + dx, y + dy);;

(** Question 3 *)
let valide (n, p) (x, y) = x >= 0 && y >= 0 && x < n && y < p;;

(** Question 4 *)
let rec filter p = function
| [] -> []
| hd::tl -> if p hd then hd :: filter p tl else filter p tl;;


(** Question 5 *)
let mouvements_valides dims pos = filter (valide dims) (map (deplace pos) mouvements);;


let get echiq (x,y) = echiq.(x).(y);;
let set echiq (x,y) valeur = echiq.(x).(y) <- valeur;;


(** Question 6 *)
exists;;

(** Question 7 *)
let resoudre dims =
  let (n, p) = dims in
  let echiquier = make_matrix n p true in
  let rec parcours nb_parcourues pos =
    nb_parcourues = n * p - 1
    || begin
      set echiquier pos false;
      let voisins = filter (get echiquier) (mouvements_valides dims pos) in
      let est_solution voisin = parcours (nb_parcourues + 1) voisin in
      exists est_solution voisins
      || (set echiquier pos true; false)
    end
  in
  parcours 0 (0, 0);;
  
(** Question 8 *)
let resoudre dims =
  let (n, p) = dims in
  let echiquier = make_matrix n p true in
  let carte = make_matrix n p (-1, -1) in
  let rec parcours nb_parcourues pos =
    nb_parcourues = n * p - 1
    || begin
      set echiquier pos false;
      let voisins = filter (get echiquier) (mouvements_valides dims pos) in
      let est_solution voisin =
        set carte pos voisin;
        parcours (nb_parcourues + 1) voisin in
      exists est_solution voisins
      || (set echiquier pos true; false)
    end
  in
  parcours 0 (0, 0);;


(** Question 9 *)
let rec chemin carte pos =
  if (-1, -1) = pos then []
  else pos :: chemin carte (get carte pos);;


let resoudre dims =
  let (n, p) = dims in
  let echiquier = make_matrix n p true in
  let carte = make_matrix n p (-1, -1) in
  let rec parcours nb_parcourues pos =
    if nb_parcourues = n * p - 1 then begin
      set carte pos (-1, -1);
      true
    end else begin
      set echiquier pos false;
      let voisins = filter (get echiquier) (mouvements_valides dims pos) in
      let est_solution voisin =
        set carte pos voisin;
        parcours (nb_parcourues + 1) voisin in
      exists est_solution voisins
      || (set echiquier pos true; false)
    end
  in
  if parcours 0 (0, 0)
  then chemin carte (0, 0) else [];;


(** Question 10 *)
let affiche_chemin (n, p) chemin =
  let carte = make_matrix n p 0 in
  let rec marque i = function
  | [] -> ()
  | hd::tl -> set carte hd i; marque (i + 1) tl in
  marque 1 chemin;
  for i = 0 to n - 1 do
    for j = 0 to p - 1 do
      print_int carte.(i).(j);
      print_char `\t`
    done;
    print_newline ()
  done;;


(** Question 11 *)
let resoudre dims =
  let (n, p) = dims in
  let echiquier = make_matrix n p true in
  let carte = make_matrix n p (-1, -1) in
  let rec parcours nb_parcourues pos =
    if nb_parcourues = n * p - 1 then begin
      set carte pos (-1, -1);
      true
    end else begin
      set echiquier pos false;
      let voisins pos = filter (get echiquier) (mouvements_valides dims pos) in
      let est_solution voisin =
        set carte pos voisin;
        parcours (nb_parcourues + 1) voisin in
      let compare_voisins vois1 vois2 =
        let voisins_libres pos = list_length (voisins pos) in
        voisins_libres vois1 <= voisins_libres vois2
      in
      exists est_solution (sort__sort compare_voisins (voisins pos))
      || (set echiquier pos true; false)
    end
  in
  if parcours 0 (0, 0)
  then chemin carte (0, 0) else [];;


(** Question 12 *)
let resoudre dims =
  let (n, p) = dims in
  let echiquier = make_matrix n p true in
  let carte = make_matrix n p (-1, -1) in
  let rec parcours nb_parcourues pos =
    if nb_parcourues = n * p - 1 then begin
      set carte pos (-1, -1);
      true
    end else begin
      set echiquier pos false;
      let voisins pos = filter (get echiquier) (mouvements_valides dims pos) in
      let est_solution voisin =
        set carte pos voisin;
        parcours (nb_parcourues + 1) voisin in
      let compare_voisins vois1 vois2 =
        let voisins_libres pos = list_length (voisins pos) in
        let vlib1, vlib2 = voisins_libres vois1, voisins_libres vois2 in
        let distance_bord (x, y) = min (x + y) (n - x + p - y) in
        vlib1 < vlib2
        || vlib1 = vlib2 && distance_bord vois1 <= distance_bord vois2
      in
      exists est_solution (sort__sort compare_voisins (voisins pos))
      || (set echiquier pos true; false)
    end
  in
  if parcours 0 (0, 0)
  then chemin carte (0, 0) else [];;


(** Question 13 *)
(fun s -> print_string s; print_char (char_of_int 34); print_string s; print_char (char_of_int 34); for i = 1 to 2 do print_char (char_of_int 59) done)"(fun s -> print_string s; print_char (char_of_int 34); print_string s; print_char (char_of_int 34); for i = 1 to 2 do print_char (char_of_int 59) done)";;

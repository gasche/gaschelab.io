\ProvidesClass{mybook}[2006/10/06 TP-Caml]
\NeedsTeXFormat{LaTeX2e}[1995/06/01]
\LoadClass[10pt]{article}

% packages
\usepackage[ec]{aeguill} 
\usepackage{bookman}
\usepackage[rflt]{floatflt}

% ============================================================================
% Dimensions
% ============================================================================

% Dimensions de la page
\oddsidemargin    -0.5in
\evensidemargin   -0.5in
\textwidth      7.5in
\headheight       1in
\topmargin        -2in
\textheight=10in

\newcommand{\mymail}{gabriel.scherer@gmail.com}
\newcommand{\mypage}{http://www.gallium.inria.fr/{\textasciitilde{}}scherer/teaching/colles/2012/}
% \newcommand{\mymail}{bluestorm.dylc@gmail.com}
% \newcommand{\mypage}{http://bluestorm.info/tmp/}

% ============================================================================
% Page de titre
% ============================================================================

\def\@tp{}
\newcommand{\tp}[2]{\def\@tp{#1}\title{#2}}

\def\maketitle%
{\noindent{\LARGE\sc TP \@tp --- \@title}%
  \hfill%
  \hbox{\texttt{\mypage{}tp\@tp.pdf}}\par%
  \vskip -8pt%
  \noindent\hrulefill\par%
  {Les questions en rapport avec les TP, Caml ou la programmation en
   g�n�ral sont les bienvenues et peuvent �tre envoy�es
   � \texttt{\mymail}. N'h�sitez pas !}%
  \vskip 12pt }

% ============================================================================
% Gestion de chapitres, sections...
% ============================================================================
   
% Num�ros de section
\def\thesection {\arabic{section}}

% Aspect de section
\def\section#1{%
  %\ifhmode\par\fi
  \removelastskip
  \vskip 5ex\goodbreak
  \refstepcounter{section}%
  %\hbox to \hsize{\hss\vbox{\advance\hsize by -1cm%
   \noindent%
   {\leavevmode\Large\raggedright%
    \thesection\ \sc#1}\par% 
   \noindent\vskip -\parskip\vskip -2ex\hrulefill%
   %}}
  \nobreak\vskip 1ex\nobreak%
  }
  
% Aspect de sous-section
\def\subsection#1{%
  \removelastskip%
  \vskip 4ex\goodbreak%
  \refstepcounter{subsection}%
   \noindent%
   {\leavevmode\large\raggedright%
    \thesubsection\ \sc#1}\par%
  \nobreak\vskip 1ex\nobreak%
  }


\def\subsubsection#1{%
  \removelastskip!%
  \vskip 4ex\goodbreak%
  \hbox to \hsize{\hss\vbox{\advance\hsize by 0.0cm
      \noindent
      {\leavevmode\large\raggedright#1}\par%
      }}%
  \nobreak\vskip 2ex\nobreak%
  }
  
% ============================================================================
% Environnements 
% ============================================================================

% Bo�te pour mettre du code
\newsavebox{\thecode}

% Bo�te d'image
\newcommand{\sideimage}[1]{%
  \begin{floatingtable}{%
      \begin{minipage}{3.21in}%
        {\includegraphics[width=3.2in]{{#1}}}%
      \end{minipage}%
}\end{floatingtable}}

% Deux images
\newcommand{\twoimages}[2]{%
  \begin{center}%
    \hfill%
    {\includegraphics[width=3.2in]{{#1}}}%
    \hfill%
    {\includegraphics[width=3.2in]{{#2}}}%
    \hfill%
  \end{center}}

% Du code non format�
\newcommand{\code}[1]{\hbox{\tt #1}}

% Bo�te de code
\newenvironment{codebox}
  {\noindent
    \vspace{2pt}%
   \begin{lrbox}{\thecode}%
   \begin{minipage}{3in}%
   \tt\small%
   }
  {\end{minipage}
   \end{lrbox}%
   \begin{floatingtable}{
   \fbox{%
    \usebox{\thecode}
    }\vspace{2pt}}
    \end{floatingtable}
   }
% Ligne de code non format�
\newenvironment{codeline}
  {\begin{codebox}}
  {\end{codebox}}

% Du code format�.
\newenvironment{codesection}
  {%
   \begin{codebox}%
   \hfill\\
   \vspace{-24pt}%
   \begin{tabbing}%
   \phantom{aa}\=\phantom{aa}\=\phantom{aa}\=%
   \phantom{aa}\=\phantom{aa}\=\phantom{aa}\=\kill%
  }
  {\end{tabbing}%%
   \end{codebox}}
   
% Le num�ro de la question
\newcounter{nbquest}

% Une question
\newenvironment{question}
  {\stepcounter{nbquest}%
    \par$\triangleright$ {\bf Question \arabic{nbquest}.} }
  {$\triangleleft$\par}

% ============================================================================
% Commandes
% ============================================================================

\newcommand{\ar}{[|}
\newcommand{\ra}{|]}

\newcommand{\li}{[}
\newcommand{\il}{]}

\newcommand{\lic}{$\colon\colon$}

\newcommand{\cat}{$\widehat{\,}\,$}
  
\newcommand{\caml}{\texttt{Caml}}
\newcommand{\camli}{\texttt{Caml Light}}



% from http://mirror.aarnet.edu.au/pub/CTAN/macros/latex/contrib/misc/boxedminipage.sty
\def\boxedminipage{\@ifnextchar [{\@iboxedminipage}{\@iboxedminipage[c]}}

\def\@iboxedminipage[#1]#2{\leavevmode \@pboxswfalse
  \if #1b\vbox
    \else \if #1t\vtop
	     \else \ifmmode \vcenter
		       \else \@pboxswtrue $\vcenter
		    \fi
	  \fi
  \fi\bgroup % start of outermost vbox/vtop/vcenter
    \hsize #2
    \hrule\@height\fboxrule
    \hbox\bgroup % inner hbox
      \vrule\@width\fboxrule \hskip\fboxsep \vbox\bgroup % innermost vbox
	\vskip\fboxsep
	\advance\hsize -2\fboxrule \advance\hsize-2\fboxsep
	\textwidth\hsize \columnwidth\hsize
	\@parboxrestore
	\def\@mpfn{mpfootnote}\def\thempfn{\thempfootnote}\c@mpfootnote\z@
	\let\@footnotetext\@mpfootnotetext
	\let\@listdepth\@mplistdepth \@mplistdepth\z@
	\@minipagerestore\@minipagetrue
	\everypar{\global\@minipagefalse\everypar{}}}

\def\endboxedminipage{%
	\par\vskip-\lastskip
	\ifvoid\@mpfootins\else
	  \vskip\skip\@mpfootins\footnoterule\unvbox\@mpfootins\fi
	\vskip\fboxsep
      \egroup % ends the innermost \vbox
      \hskip\fboxsep \vrule\@width\fboxrule
    \egroup % ends the \hbox
    \hrule\@height\fboxrule
  \egroup% ends the vbox/vtop/vcenter
  \if@pboxsw $\fi}

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define BUFLEN 100

char *
getword(FILE *f)
{
    char buf[BUFLEN];
    int c, i;

    do {
        c = fgetc(f);
        if(c == EOF)
            return NULL;
    } while(!isalpha(c));

    buf[0] = tolower(c);
    i = 1;

    while(1) {
        c = fgetc(f);
        if(c == EOF)
            return NULL;
        if(!isalpha(c))
            break;
        buf[i++] = tolower(c);
        if(i >= BUFLEN-1)
            return NULL;
    }
    buf[i] = '\0';
    return strdup(buf);
}

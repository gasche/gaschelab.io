# May 2018 OCaml DocJam

The May 2018 OCaml DocJam is a distributed/remote event happening on
**May 18, 19, 20**, encouraging interested members of the OCaml
community to collectively improve the documentation of OCaml programs
and libraries by proposing improvements to the documentation of OCaml
projects.

## When

The event is organized in May 2018, on Friday 18, Saturday 19 and
Sunday 20, anywhere on earth. One or two hours of your time during
that period should be enough to make a valuable contribution.

## Contribution protocol

We suggest the following protocol for each participant. For details,
see the "Details" section below.

- Decide on a project to look at, typically a project that you have
  used as a beginner in the not-so-distant past (we will discuss
  project suggestions during the event).

- Perform a small improvement to its documentation, or a series of
  such improvement; if you apply patches to the project, they should
  get into a fresh branch of your clone of the repository.

- *Optionally*, request some feedback on your proposed change to other
  participants: create a pull-request from your fresh branch to the
  master branch *of your clone*, and post it on the DocJam channels to
  ask for feedback.

- Once you are satisfied with your patches, send it to the maintainers
  of the project. Log the submission in the [DocJam
  log]](https://discuss.ocaml.org/t/may-2018-ocaml-docjam-log/2018)
  that collects our Jam actions.

## Where

This is an online event with participants at remote physical
locations -- of course, interested participants are encouraged to
gather in physical spaces for conviviality.

The following online spaces will be used to organize, discuss and
organize our contributions:

- the Discuss thread [May 2018 OCaml DocJam Thread
  ](https://discuss.ocaml.org/t/may-2018-ocaml-docjam-thread-may-18-19-20/1957)
  is a general point of discussion for the Jam; feel free to ask any
  questions there

- the Discuss thread [May 2018 OCaml DocJam
  Log](https://discuss.ocaml.org/t/may-2018-ocaml-docjam-log/2018)
  will serve as the log of Jam actions; post there each documentation
  contribution that you submit to upstream project maintainers during
  the Jam.

- docjam channels may be active on several instant communication platforms:
  - IRC: the #ocaml channel on Freenode
    ([link to a web client](https://webchat.freenode.net/#ocaml),
     [irc link](irc://irc.freenode.net/#ocaml))
  - The `#doc` room on the [OCaml Discord Channel](https://discord.gg/cCYQbqN)
  - TODO: link to the #ocaml room on Matrix/Riot?

## Details

Improvements may concern, for example, the following:

- "item comments", the description of individual library declarations
  or project options/features

- "global comments", the general description of a project

- "code examples", providing illustrative and reusable examples
  of the use of a library, or of a specific feature

- "deployment", improving the access to the documentation by its users,
  for example by setting up an online version of the documentation
  and that the project webpage clearly advertises this documentation,
  by making sure that relevant interface files and build artifact are
  installed on user systems, that it is compatible with documentation
  tools such as `odoc`, `odig` or `ocp-browser`.

### Advice on how to write documentation?

The [OCaml Documentation
Guideline](https://github.com/bobbypriambodo/ocaml-documentation-guideline)
contains advice on how to write documentation for OCaml projects.

### Where to find ideas of project to improve?

The [awesome-ocaml](https://github.com/rizo/awesome-ocaml/) project
has grown into a big list of interesting OCaml projects, sorted by
topic/area, and may be a source of inspiration if you are looking for
documentation targets.

In general we recommend that you pick a library that you are
interested in, but for which you are not already an expert. The docjam
is actually a great way to get started on a given library; the
beginner perspective is useful to write good documentation.

Feel free to also ask other participants for suggestions, or work on
reviewing other participants' proposals.
